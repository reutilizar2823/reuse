package com.example.rishabh.reutilizar.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.egistli.droidimagepicker.ImagePicker;
import com.egistli.droidimagepicker.ImagePickerDelegate;
import com.egistli.droidimagepicker.ImagePickerError;
import com.example.rishabh.reutilizar.Constants;

import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.R;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mehdi.sakout.fancybuttons.FancyButton;


public class AdditionalBookDetailsActivity extends AppCompatActivity implements View.OnClickListener, ImagePickerDelegate {
    private OneBookDetail oneBookDetail;
    private EditText titleEditText,descEditText;
    private FancyButton submitButton , setImageButton , removeImageButton;
    private ImageView oneBookImageView;
    private String imagePath="";
    int position;
    ImagePicker imagePicker;
    File photoFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_book_details);
        // Ignore URI exposed Exception
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setTitle("Additional Details");
        Intent i = getIntent();
        oneBookDetail= (OneBookDetail) i.getSerializableExtra(Constants.ONE_BOOK_DETAILS);
        titleEditText= (EditText) findViewById(R.id.one_book_title_editText);
        descEditText=(EditText) findViewById(R.id.one_book_description_editText);
        submitButton=(FancyButton) findViewById(R.id.add_extra_details_button);
        oneBookImageView = (ImageView) findViewById(R.id.one_book_image);
        setImageButton =  findViewById(R.id.set_book_image);
        removeImageButton =  findViewById(R.id.remove_book_image);
        setImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               selectImage();
            }
        });
        removeImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                oneBookImageView.setImageResource(R.drawable.default_book);
                imagePath="";
            }
        });
        position=i.getIntExtra("Position",0);

        oneBookImageView.setImageResource(R.drawable.default_book);
        oneBookImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        titleEditText.setText(oneBookDetail.title);
        descEditText.setText(oneBookDetail.description);

        titleEditText.setEnabled(!oneBookDetail.owner);
        imagePath = oneBookDetail.bookPath;

        if(!imagePath.equals("")){
            setPic();
        }




        submitButton.setOnClickListener(this);
    }

    private void selectImage() {
        boolean result=Utility.checkPermissionCamera(AdditionalBookDetailsActivity.this);
        if(result)
            imageIntent();
    }


    private void imageIntent() {
        imagePicker = new ImagePicker(AdditionalBookDetailsActivity.this ,  this, 2000);
        imagePicker.prompt();
    }


    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imagePath = image.getAbsolutePath();
        return image;

    }



    @Override
    public void onClick(View view) {
        if (titleEditText.getText() == null) {
            titleEditText.setError("Can't be Empty");
            return;
        }
        if (titleEditText.getText().toString().trim().isEmpty()) {
            titleEditText.setError("Can't be Empty");
            return;
        }
        if (!oneBookDetail.owner) {
            oneBookDetail.title = titleEditText.getText().toString().trim();
        }
        if (descEditText.getText() != null) {
            oneBookDetail.description = descEditText.getText().toString().trim();
        }
        oneBookDetail.bookPath = imagePath;
        Intent intent = new Intent();
        intent.putExtra("Position" , position);
        intent.putExtra(Constants.ONE_BOOK_DETAILS, oneBookDetail);
        setResult(RESULT_OK , intent);
        finish();

    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (imagePicker != null && imagePicker.handleActivityResult(requestCode, resultCode, data)) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);



    }

    private void setPic() {
        File file = new File(imagePath);
        if(file.exists()) {
            Glide.with(this).load(imagePath).apply(RequestOptions.noTransformation())
                    .into(oneBookImageView);
            // oneBookImageView.setPadding(0, 0, 0, 0);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imageIntent();
                } else {
                    //code for deny
                }
                break;
            case Utility.MY_PERMISSIONS_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean storagePermsission = Utility.checkPermissionReadExternalStorage(AdditionalBookDetailsActivity.this);
                    if(storagePermsission) {
                        imageIntent();
                    }
                } else {
                    //code for deny
                }
                break;
        }
    }


    @Override
    public void imagePickerDidCancel(ImagePicker imagePicker) {

    }

    @Override
    public void imagePickerDidSelectImage(ImagePicker imagePicker, Bitmap bitmap) {
        photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(photoFile==null){
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(photoFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            Log.d("Exception", "File not found: " + e.getMessage());
        }
        oneBookImageView.setImageBitmap(bitmap);
    }

    @Override
    public void imagePickerDidFailToSelectImage(ImagePicker imagePicker, ImagePickerError error) {

    }

    @Override
    public boolean imagePickerShouldCrop(ImagePicker imagePicker) {
        return true;
    }

    @Override
    public void imagePickerSetUpCropDetail(ImagePicker imagePicker, Crop crop) {
        crop.asSquare();
        crop.withMaxSize(480 , 480);

    }
}
class Utility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_CAMERA = 100;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionReadExternalStorage(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermissionCamera(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_CAMERA);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_CAMERA);
                }
                return false;
            } else {
                return checkPermissionReadExternalStorage(context);
            }
        } else {
            return true;
        }
    }
}