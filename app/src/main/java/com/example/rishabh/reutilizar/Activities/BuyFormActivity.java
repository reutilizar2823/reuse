package com.example.rishabh.reutilizar.Activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;


import android.widget.TextView;
import android.widget.Toast;


import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.example.rishabh.reutilizar.SpinnerDialog.OnSpinnerItemClick;
import com.example.rishabh.reutilizar.SpinnerDialog.SpinnerDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;


import java.util.ArrayList;
import java.util.Arrays;


public class BuyFormActivity extends AppCompatActivity implements View.OnClickListener {
    private String className,schoolName,residentialAddress;
    private String lat,lng;
    private Button buyFormSubmitButton;
    private TextView schoolTextView , classTextView , locationTextView;
    private SpinnerDialog classSpinnerDialog , schoolSpinnerDialog;
    private String[] schoolArray , classArray;
    private ArrayList<String> schoolArrayList , classArrayList;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        setTitle("Search Sellers");
        schoolTextView = findViewById(R.id.form_school_textView);
        classTextView = findViewById(R.id.form_class_textView);
        locationTextView = findViewById(R.id.form_location_textView);

        schoolArray = getResources().getStringArray(R.array.school_name_list);
        schoolArrayList = new ArrayList<>(Arrays.asList(schoolArray));
        schoolSpinnerDialog = new SpinnerDialog(this , schoolArrayList , "Select your school" );

        classArray = getResources().getStringArray(R.array.class_spinner_items);
        classArrayList = new ArrayList<>(Arrays.asList(classArray));
        classSpinnerDialog = new SpinnerDialog(this , classArrayList , "Select your class" );

        schoolTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                schoolSpinnerDialog.showSpinerDialog();
            }
        });

        classTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classSpinnerDialog.showSpinerDialog();
            }
        });

        schoolSpinnerDialog.bindOnSpinerListener(new OnSpinnerItemClick() {
            @Override
            public void onClick(String item, int position) {
                schoolTextView.setText(item);
            }
        });

        classSpinnerDialog.bindOnSpinerListener(new OnSpinnerItemClick() {
            @Override
            public void onClick(String item, int position) {
                classTextView.setText(item);
            }
        });


        buyFormSubmitButton=findViewById(R.id.form_submit_button);
        buyFormSubmitButton.setOnClickListener(this);
        locationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setCountry("IN")
                        .build();
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .setFilter(typeFilter)
                            .build(BuyFormActivity.this);
                    startActivityForResult(intent , PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private boolean isAddressContainsPlace(String place , String address){
        int i=0;
        if(address.length()<place.length()){
            return false;
        }
        while(i<place.length()){
            if(place.charAt(i)==address.charAt(i)){
                i++;
            }
            else {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if( schoolTextView.getText()==null || schoolTextView.getText().toString().equals("Enter your School")){
            Toast.makeText(this , "School can't be empty!" , Toast.LENGTH_SHORT).show();
            return;
        }

        if(classTextView.getText()==null || classTextView.getText().toString().equals("Enter your Class")){
            Toast.makeText(this , "Class can't be empty!" , Toast.LENGTH_SHORT).show();
            return;
        }
        if( locationTextView.getText()==null || locationTextView.getText().toString().equals("Enter your Location")){
            Toast.makeText(this , "Location can't be empty!" , Toast.LENGTH_SHORT).show();
                return;
        }
        if(!Client.haveNetworkConnection(this)){
            Snackbar.make(findViewById(R.id.form_main_layout),"No Internet Connection",Snackbar.LENGTH_SHORT).show();
             return;
        }
        className = classTextView.getText().toString();
        schoolName = schoolTextView.getText().toString();
        UserRequestDetails userRequestDetails = new UserRequestDetails(schoolName,className,residentialAddress,lat,lng);
        Intent intent = new Intent(this,PotentialSellersActivity.class);
        intent.putExtra(Constants.BUY_DETAILS, userRequestDetails);
        intent.putExtra(Constants.LAUNCHING_ACTIVITY,Constants.GENERAL_ACTIVITY);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PLACE_AUTOCOMPLETE_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                Place place = PlaceAutocomplete.getPlace(this , data);
                String placeName =(String)place.getName();
                String addressName = (String)place.getAddress();
                if(isAddressContainsPlace(placeName ,  addressName)){
                    residentialAddress = addressName;
                }else {
                    residentialAddress = placeName + " " + addressName;
                }
                locationTextView.setText(residentialAddress);
                LatLng latLng = place.getLatLng();
                lat=Double.toString(latLng.latitude);
                lng=Double.toString(latLng.longitude);
            }
        }
    }
}
