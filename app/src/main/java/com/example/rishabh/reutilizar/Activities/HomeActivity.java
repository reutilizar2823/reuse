package com.example.rishabh.reutilizar.Activities;

import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Models.User;
import com.example.rishabh.reutilizar.R;
import com.example.rishabh.reutilizar.Adapters.SectionPagerAdapter;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private SectionPagerAdapter mSectionPagerAdapter;
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("ReuseBook");
        SharedPreferences name_shared_preference = Client.getSharedPreferences(this);
        name = name_shared_preference.getString(Constants.NAME , null);
        User.setName(name);
        User.setNumber(Client.getmFirebaseUser().getPhoneNumber());
        User.setUID(Client.getmFirebaseUser().getUid());
        User.setPhotoURL("");

        mSectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(0);
        mViewPager.setAdapter(mSectionPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sell) {
            startActivity(new Intent(this, SellFormActivity.class));
        } else if (id == R.id.nav_buy) {
            startActivity(new Intent(this, BuyFormActivity.class));
        } else if (id == R.id.nav_notification) {
            startActivity(new Intent(this , NotificationActivity.class) );
        } else if (id == R.id.nav_my_account) {
            Intent intent = new Intent(this,ProfileActivity.class);
            if(name==null){
                name="ABC";
            }
            intent.putExtra("name",name);
            startActivity(intent);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_faq) {

        }else if(id == R.id.report_us){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SENDTO);
            Uri uri = Uri.parse("mailto:reusebookofficial@gmail.com");
            sendIntent.putExtra(Intent.EXTRA_SUBJECT , "Report us : ReuseBook");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "My Uid is : " +
                    Client.getmFirebaseUser().getUid() +"\n\n" + "Write your Message :");
            sendIntent.setData(uri);
            if (sendIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(sendIntent);
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.notification_menu){
            startActivity(new Intent(this , NotificationActivity.class) );
        }
        return true;
    }




}
