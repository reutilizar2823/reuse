package com.example.rishabh.reutilizar.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.rishabh.reutilizar.Activities.MainActivity;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NoInternetConnection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.no_internet_connection);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Client.haveNetworkConnection(this)){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
    }
}
