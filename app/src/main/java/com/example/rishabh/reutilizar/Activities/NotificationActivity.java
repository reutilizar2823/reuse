package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.example.rishabh.reutilizar.Adapters.NotificationAdAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.DatabaseConstants;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.NotificationAdDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity implements NotificationAdAdapter.onNotificationClicked {
    private RecyclerView recyclerView ;
    private NotificationAdAdapter adapter;
    private ArrayList<NotificationAdDetails> arrayList ;
    private ProgressDialog dialog;
    private int count,totalNewSellersCount;
    private  android.support.v7.app.ActionBar actionBar;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        actionBar= getSupportActionBar();
        actionBar.setTitle("Notifications");
        linearLayout = findViewById(R.id.no_new_buy_ad_linearLayout);
        recyclerView =  findViewById(R.id.notification_recyclerview);
        arrayList = new ArrayList<>();
        adapter = new NotificationAdAdapter(this,arrayList , this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        dialog = new ProgressDialog(NotificationActivity.this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if(keyCode==keyEvent.KEYCODE_BACK && !keyEvent.isCanceled()){
                    if(dialog.isShowing()){
                        dialog.dismiss();
                        finish();
                    }
                    return  true;
                }
                return false;
            }
        });
         new  AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog.setMessage("Fetching notification....");
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            protected Void doInBackground(Void... voids) {
                fetchArrayList();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(arrayList.size()==0){
                    if(dialog!=null && dialog.isShowing()) dialog.dismiss();
                    linearLayout.setVisibility(View.VISIBLE);

                    return;
                }

                for(final NotificationAdDetails notificationAdDetails : arrayList){
                    String schoolName = notificationAdDetails.getSchoolName();
                    String className = notificationAdDetails.getClassName();
                    final String lastSeenTimeStamp = notificationAdDetails.getLastSeenTimeStamp();
                    DatabaseReference reference = Client.getmSchoolDatabaseReference().child(schoolName)
                            .child(className).child("sell");
                    reference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Iterable<DataSnapshot> userDataSnapshot = dataSnapshot.getChildren();
                            for(DataSnapshot oneUserDataSnapshot : userDataSnapshot){
                                Iterable<DataSnapshot>  userDataSnap = oneUserDataSnapshot.getChildren();
                                for(DataSnapshot onesellDatasnapshot : userDataSnap ){
                                    if(!onesellDatasnapshot.getKey().equals("name") &&
                                            !onesellDatasnapshot.getKey().equals("number")&&
                                            !onesellDatasnapshot.getKey().equals("photoUrl")){
                                        Object sellTimeStampObj = onesellDatasnapshot.child("timeStamp").getValue();
                                        if(sellTimeStampObj==null){
                                            continue;
                                        }
                                        String sellTimeStamp = sellTimeStampObj.toString();
                                        if(Long.parseLong(sellTimeStamp)>=Long.parseLong(lastSeenTimeStamp)){
                                            totalNewSellersCount++;
                                            int c = notificationAdDetails.getCount()+1;
                                            notificationAdDetails.setCount(c);
                                        }
                                    }
                                }

                            }
                            adapter.notifyDataSetChanged();
                            count++;
                            if(count==arrayList.size()){
                                if(totalNewSellersCount==0){
                                    actionBar.setSubtitle("No new seller found");
                                }else if(totalNewSellersCount==1){
                                    actionBar.setSubtitle(totalNewSellersCount+" new seller found");
                                }else{
                                    actionBar.setSubtitle(totalNewSellersCount+" new sellers found");
                                }
                                if(dialog!=null && dialog.isShowing())
                                    dialog.dismiss();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if(dialog!=null && dialog.isShowing()) dialog.dismiss();
                        }
                    });
                }

            }
        }.execute();
    }

    private void fetchArrayList() {
        SQLiteDatabase database = DatabaseClient.getReadableDatabase(this);
        String[] selectionArgs = {DatabaseConstants.BUY_CATEGORY};
        Cursor cursor = database.query(OpenHelper.USER_SELLBUY_TABLE_NAME , null ,OpenHelper.CATEGORY + " = ? " , selectionArgs , null , null , null );
        while(cursor.moveToNext()){
            String schoolName = cursor.getString(cursor.getColumnIndex(OpenHelper.SCHOOL));
            String className = cursor.getString(cursor.getColumnIndex(OpenHelper.CLASS));
            String location = cursor.getString(cursor.getColumnIndex(OpenHelper.LOCATION));
            String lat = cursor.getString(cursor.getColumnIndex(OpenHelper.LATITUDE));
            String lng = cursor.getString(cursor.getColumnIndex(OpenHelper.LONGITUDE));
            String buyId = cursor.getString(cursor.getColumnIndex(OpenHelper.SELLBUY_ID));
            String timeStamp = cursor.getString(cursor.getColumnIndex(OpenHelper.TIME_STAMP));
            String lastSeenTimeStamp = cursor.getString(cursor.getColumnIndex(OpenHelper.LAST_SEEN_TIME_STAMP));
            NotificationAdDetails notificationAdDetails = new NotificationAdDetails(schoolName,
                    className,location,lat,lng,buyId , 0, lastSeenTimeStamp ,timeStamp);
            arrayList.add(notificationAdDetails);
        }
        cursor.close();
    }

    @Override
    public void onClicked( int position) {
        NotificationAdDetails notificationAdDetails = arrayList.get(position);
        long lastSeenTimeStamp = Long.parseLong(arrayList.get(position).getLastSeenTimeStamp());
        SQLiteDatabase database = DatabaseClient.getWritableDatabase(this);
        String timeStamp = String.valueOf(System.currentTimeMillis());
        ContentValues cv = new ContentValues();
        cv.put(OpenHelper.LAST_SEEN_TIME_STAMP , timeStamp);
        String[] selectionArgs = {arrayList.get(position).getSell_buyId()};
        database.update(OpenHelper.USER_SELLBUY_TABLE_NAME , cv , OpenHelper.SELLBUY_ID + " = ? " , selectionArgs);
        Intent intent = new Intent(this,PotentialSellersActivity.class);
        intent.putExtra("lastSeenTimeStamp" , lastSeenTimeStamp);
        intent.putExtra("notification",notificationAdDetails);
        intent.putExtra(Constants.LAUNCHING_ACTIVITY,Constants.NOTIFICATION_ACTIVITY);
        startActivity(intent);
        finish();

    }

  }

