package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rishabh.reutilizar.Adapters.BookDetailsRecyclerAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.DatabaseConstants;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.Models.User;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;

public class PartialBookSetActivity extends AppCompatActivity implements BookDetailsRecyclerAdapter.BookViewClickListener, View.OnClickListener {
    private RecyclerView bookDetailsRecyclerView;
    private ArrayList<OneBookDetail> oneBookDetailsArrayList;
    private BookDetailsRecyclerAdapter bookDetailsRecyclerAdapter;
    private static final int REQUEST_CODE=10;
    private FloatingActionButton fab;
    private String sellId;
    UserRequestDetails userRequestDetails;
    private String price="0";
    private ProgressDialog progressDialog ;
    private MenuItem menuItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partial_book_set);
        setTitle("Books Details");
        Intent intent = getIntent();
        userRequestDetails = (UserRequestDetails)intent.getSerializableExtra(Constants.SELL_DETAILS);
        oneBookDetailsArrayList=new ArrayList<>();
        fab=(FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(this);
        for(int i=1;i<=10;i++){
            OneBookDetail oneBookDetail = new OneBookDetail();
            oneBookDetail.title="Title : "+i;
            oneBookDetail.checkBox=false;
            oneBookDetail.condition=Constants.getWorstBookCondition();
            oneBookDetail.owner=true;
            oneBookDetailsArrayList.add(oneBookDetail);
        }
        bookDetailsRecyclerView=(RecyclerView)findViewById(R.id.book_details_recycler_view);
        bookDetailsRecyclerAdapter=new BookDetailsRecyclerAdapter(this,oneBookDetailsArrayList,this);
        bookDetailsRecyclerView.setAdapter(bookDetailsRecyclerAdapter);
        bookDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        bookDetailsRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        bookDetailsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Posting...");
    }

    @Override
    public void onMoreInfoButtonClicked(View view, int position) {
        OneBookDetail oneBookDetail = oneBookDetailsArrayList.get(position);
        Intent intent = new Intent(this,AdditionalBookDetailsActivity.class);
        intent.putExtra(Constants.ONE_BOOK_DETAILS,oneBookDetail);
        intent.putExtra("Position",position);
        startActivityForResult(intent,REQUEST_CODE);

    }

    @Override
    public void onLongClicked(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are You Sure");
        builder.setMessage("This Book Detail will be deleted");
        builder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                oneBookDetailsArrayList.remove(position);
                bookDetailsRecyclerAdapter.notifyItemRemoved(position);
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==REQUEST_CODE){
            if(resultCode==RESULT_OK){
                OneBookDetail oneBookDetail = (OneBookDetail) data.getSerializableExtra(Constants.ONE_BOOK_DETAILS);
                int position = data.getIntExtra("Position" , 0);
                oneBookDetailsArrayList.set(position,oneBookDetail);
                if(!oneBookDetail.owner){
                    bookDetailsRecyclerAdapter.notifyItemChanged(position);
                }

            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.fab){
            showDialog();
        }
    }

    private void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
       // builder.setTitle("Add New Book");
        View v =getLayoutInflater().inflate(R.layout.dialog_book_view,null);
        builder.setView(v);
        final EditText titleEditText =(EditText)v.findViewById(R.id.dialog_book_title_editText);


        builder.setPositiveButton("ADD" ,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(titleEditText.getText()==null){
                    titleEditText.setError("Can't Be Empty");
                    return;
                }
                if(titleEditText.getText().toString().trim().isEmpty()){
                    titleEditText.setError("Can't Be Empty");
                    return;
                }
                OneBookDetail oneBookDetail=new OneBookDetail();
                oneBookDetail.title=titleEditText.getText().toString().trim();
                oneBookDetail.owner=false;
                oneBookDetail.checkBox=true;
                oneBookDetail.condition=Constants.getWorstBookCondition();
                oneBookDetailsArrayList.add(oneBookDetail);
                bookDetailsRecyclerAdapter.notifyItemInserted(oneBookDetailsArrayList.size());
                bookDetailsRecyclerView.smoothScrollToPosition(oneBookDetailsArrayList.size());



            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();

        dialog.show();
        Button positiveButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        if(positiveButton!=null){
            positiveButton.setTextColor(Color.BLACK);
        }
        if(negativeButton!=null){
            negativeButton.setTextColor(Color.BLACK);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sell_book_details_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuItem = item;
        if(item.getItemId()==R.id.submit_all_books_menu_button){
            int count=0;
            for(OneBookDetail o : oneBookDetailsArrayList){
                if(o.checkBox){
                    count++;
                }
            }
            if(count==0){
                Snackbar.make(findViewById(R.id.partial_book_layout) , "At least one Book required",Snackbar.LENGTH_SHORT).show();
                return false;
            }
            else if(count>30){
                Snackbar.make(findViewById(R.id.partial_book_layout) , "You cannot post more than 30 books",Snackbar.LENGTH_SHORT).show();
                return false;
            }
            menuItem.setEnabled(false);
            enterPrice();
        }
        return true;
    }

    private void enterPrice() {
        View v =getLayoutInflater().inflate(R.layout.dialog_price_view,null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(v)
                .setTitle("Add Price")
                .setPositiveButton("SUBMIT",null)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        menuItem.setEnabled(true);
                        dialogInterface.dismiss();
                    }
                }).setCancelable(false)
                .create();

        final EditText editText = v.findViewById(R.id.dialog_price_view_editText);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(editText.getText()!=null && editText.getText().toString().length()!=0){
                            int flag = checkPrice(editText.getText().toString());
                            if(flag==1){
                               editText.setError("Enter valid Amount");
                            }else if(flag==2){
                               editText.setError("Price must less than or equal to 3000");
                            }
                            else{
                                String price1 = editText.getText().toString();
                                int priceInt = Integer.parseInt(price1);
                                price = String.valueOf(priceInt);
                                dialogInterface.dismiss();
                                progressDialog.show();
                                submitDataTask();
                            }
                        }else{
                            menuItem.setEnabled(true);
                            dialogInterface.dismiss();
                        }
                    }
                });
            }
        });
        dialog.show();

    }

    private int checkPrice(String s) {
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)>'9' || s.charAt(i)<'0'){
                return 1;
            }
        }
        if(s.length()>4){
            return  1;
        }
        int price = Integer.parseInt(s);
        if(price<0 || price > 3000){
            return  2;
        }
        return  3;
    }

    private void submitDataTask() {

        new AsyncTask<Void, Void, Boolean>() {


            @Override
            protected Boolean doInBackground(Void... voids) {
                //PUSH IN FIREBASE
                if(User.getUID()!=null){

                    DatabaseReference dbReference = Client.getmSchoolDatabaseReference().child(userRequestDetails.getSchoolName())
                            .child(userRequestDetails.getClassName()).child("sell").child(Client.getmFirebaseUser().getUid());
                    sellId = dbReference.push().getKey();
                    //PUSH IN LOCAL DATABASE
                    SQLiteDatabase database = DatabaseClient.getWritableDatabase(getApplicationContext());
                    ContentValues cv = new ContentValues();
                    cv.put(OpenHelper.SCHOOL, userRequestDetails.getSchoolName());
                    cv.put(OpenHelper.CLASS, userRequestDetails.getClassName());
                    cv.put(OpenHelper.LOCATION, userRequestDetails.getLocation());
                    cv.put(OpenHelper.LATITUDE, userRequestDetails.getLat());
                    cv.put(OpenHelper.LONGITUDE, userRequestDetails.getLng());
                    cv.put(OpenHelper.SELLBUY_ID, sellId);
                    cv.put(OpenHelper.CATEGORY, DatabaseConstants.SELL_CATEGORY);
                    cv.put(OpenHelper.STATUS, DatabaseConstants.PARTIAL_BOOK_STATUS);
                    String timeStamp = String.valueOf(System.currentTimeMillis());
                    cv.put(OpenHelper.TIME_STAMP , timeStamp);
                    cv.put(OpenHelper.LAST_SEEN_TIME_STAMP , "");
                    cv.put(OpenHelper.PRICE_BOOK_SET,price);
                    database.insert(OpenHelper.USER_SELLBUY_TABLE_NAME, null, cv);

                    int count=0;
                    for (OneBookDetail oneBookDetail : oneBookDetailsArrayList) {
                        if (oneBookDetail.checkBox) {
                            String bookId = count+"";
                            oneBookDetail.bookId = bookId;
                            count++;
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(OpenHelper.SELL_ID, sellId);
                            contentValues.put(OpenHelper.TITLE, oneBookDetail.title);
                            contentValues.put(OpenHelper.CONDITION, oneBookDetail.condition);
                            contentValues.put(OpenHelper.DESCRIPTION, oneBookDetail.description);
                            contentValues.put(OpenHelper.IMAGE_URL, oneBookDetail.bookUrl);
                            contentValues.put(OpenHelper.IMAGE_PATH , oneBookDetail.bookPath);
                            contentValues.put(OpenHelper.BOOK_ID , bookId);
                            database.insert(OpenHelper.BOOK_DETAILS_TABLE_NAME, null, contentValues);
                        }
                    }

                    //PUSH IN Firebase
                    DatabaseReference userDBReferenceClient = Client.getmUserDatabaseReference().
                            child(Client.getmFirebaseUser().getUid())
                            .child("sell").child(sellId);
                    userDBReferenceClient.setValue(userRequestDetails);
                    userDBReferenceClient.child("timeStamp").setValue(timeStamp);
                    userDBReferenceClient.child("price").setValue(price);
                    userDBReferenceClient.child("status").setValue(DatabaseConstants.PARTIAL_BOOK_STATUS) ;

                    dbReference.child("name").setValue(User.getName());
                    dbReference.child("number").setValue(User.getNumber());
                    dbReference.child("photoUrl").setValue(User.getPhotoURL());

                    DatabaseReference sellIdDatabaseReference = dbReference.child(sellId);
                    sellIdDatabaseReference.child("location" ).setValue(userRequestDetails.getLocation());
                    String lat = userRequestDetails.getLat();
                    String lng = userRequestDetails.getLng();
                    sellIdDatabaseReference.child("lat").setValue(lat);
                    sellIdDatabaseReference.child("lng").setValue(lng);
                    sellIdDatabaseReference.child("timeStamp").setValue(timeStamp);
                    sellIdDatabaseReference.child("price").setValue(price);

                    for(OneBookDetail o : oneBookDetailsArrayList) {
                        if (o.checkBox) {
                            DatabaseReference bookDbRef = sellIdDatabaseReference.child(o.bookId);
                            HashMap<String, String> map = new HashMap<>();
                            map.put("title", o.title);
                            map.put("description", o.description);
                            map.put("condition", o.condition);
                            map.put("bookId" , o.bookId);
                            map.put("bookUrl" , o.bookUrl);
                            bookDbRef.setValue(map);
                        }
                    }
                    sellIdDatabaseReference.child("status").setValue("partial");
                    /*DatabaseReference userDBReferenceClient = Client.getmUserDatabaseReference().
                            child(Client.getmFirebaseUser().getUid())
                            .child("sell").child(sellId);
                    userDBReferenceClient.setValue(userRequestDetails);
                    userDBReferenceClient.child("timeStamp").setValue(timeStamp);
                    userDBReferenceClient.child("price").setValue(price);
                    userDBReferenceClient.child("status").setValue(DatabaseConstants.PARTIAL_BOOK_STATUS) ;*/

                }else{
                    return false;
                }



                return true;
            }

            @Override
            protected void onPostExecute(Boolean bool) {
                super.onPostExecute(bool);

                if(!bool){
                    if(progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(PartialBookSetActivity.this, "Error Occurred!Please Restart your App", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent=new Intent(PartialBookSetActivity.this,PreviewSellAd.class);
                intent.putExtra("partialActivity" , Constants.PARTIAL_BOOK_ACTIVITY);
                intent.putExtra("sellId",sellId);
                intent.putExtra("userDetails" , userRequestDetails);
                if(progressDialog!=null && progressDialog.isShowing())
                       progressDialog.dismiss();
                startActivity(intent);

            }
        }.execute();

    }
}
