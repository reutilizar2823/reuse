package com.example.rishabh.reutilizar.Activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.rishabh.reutilizar.Adapters.BuyerDetailsAdapter;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Fragment.PotentialBuyerFragment;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;

public class PotentialBuyerActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potential_buyer);
        setTitle("Potential Buyers");
        Intent intent = getIntent();
        UserRequestDetails userRequestDetails = (UserRequestDetails)intent.getSerializableExtra(Constants.SELLER_DETAILS);
        PotentialBuyerFragment potentialBuyerFragment= new PotentialBuyerFragment();
        Bundle bundle= new Bundle();
        bundle.putSerializable(Constants.SELLER_DETAILS,userRequestDetails);
        potentialBuyerFragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.buyer_container,potentialBuyerFragment).commit();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
