package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rishabh.reutilizar.Adapters.SellerDetailsAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.DatabaseConstants;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.NotificationAdDetails;
import com.example.rishabh.reutilizar.Models.SellerDistanceSort;
import com.example.rishabh.reutilizar.Models.SellersBasicInfo;
import com.example.rishabh.reutilizar.Models.ShowBookDetail;
import com.example.rishabh.reutilizar.Models.SellerTimeSort;
import com.example.rishabh.reutilizar.Models.User;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

public class PotentialSellersActivity extends AppCompatActivity implements SellerDetailsAdapter.SellerViewClickListener, View.OnClickListener {

    private UserRequestDetails userRequestDetails;
    private NotificationAdDetails notificationAdDetails;
    private DataSnapshot mDataSnapshot;
    private ArrayList<SellersBasicInfo> sellersBasicInfoArrayList;
    ProgressBar mProgressBar;
    RecyclerView potentialSellerRecyclerView;
    SellerDetailsAdapter sellerDetailsAdapter;
    LinearLayout notifyLinearLayout;
    ImageView notificationImageView;
    int activity_code;
    ProgressDialog progressDialog;
    private LinearLayout noSellerLinearLayout;
    private boolean isBellClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potential_sellers);
        setTitle("Sellers Details");
        if(!Client.haveNetworkConnection(this)){
            Snackbar.make(findViewById(R.id.potential_seller_layout),"No Internet Connection",
                    Snackbar.LENGTH_LONG).show();
            return;
        }
        noSellerLinearLayout = findViewById(R.id.no_seller_linearLayout);
        sellersBasicInfoArrayList=new ArrayList<>();
        mProgressBar=findViewById(R.id.potential_seller_progessBar);
        showProgress(true);
        potentialSellerRecyclerView=findViewById(R.id.potential_seller_recyclerView);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Posting Buy Ad");
        progressDialog.setCancelable(false);
        Intent intent = getIntent();
        activity_code = intent.getIntExtra(Constants.LAUNCHING_ACTIVITY,0);
        if(activity_code==Constants.GENERAL_ACTIVITY){
            sellerDetailsAdapter=new SellerDetailsAdapter(this,sellersBasicInfoArrayList,this);
            potentialSellerRecyclerView.setAdapter(sellerDetailsAdapter);
            potentialSellerRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
            potentialSellerRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
            potentialSellerRecyclerView.setItemAnimator(new DefaultItemAnimator());

            userRequestDetails = (UserRequestDetails) intent.getSerializableExtra(Constants.BUY_DETAILS);
            notifyLinearLayout=findViewById(R.id.notify_layout);
            notificationImageView=findViewById(R.id.notify_imageView);
            notificationImageView.setOnClickListener(this);

            final DatabaseReference  dbReference = Client.getmSchoolDatabaseReference().child(userRequestDetails.getSchoolName())
                    .child(userRequestDetails.getClassName()).child("sell");

            ValueEventListener mListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mDataSnapshot = dataSnapshot;
                    Iterable<DataSnapshot> userDatasnapshot = dataSnapshot.getChildren();
                    if(dataSnapshot.getChildrenCount()==0){
                        notifyLinearLayout.setVisibility(View.VISIBLE);
                        showProgress(false);
                        noSellerLinearLayout.setVisibility(View.VISIBLE);
                        return ;
                    }
                    for(DataSnapshot oneUserDatasnapshot : userDatasnapshot ){
                        String uid = oneUserDatasnapshot.getKey();
                        Object nameObj = oneUserDatasnapshot.child("name").getValue();
                        if(nameObj==null){
                            continue;
                        }
                        String name = nameObj.toString();
                        Object numberObj = oneUserDatasnapshot.child("number").getValue();
                        if(numberObj==null){
                            continue;
                        }
                        String number = numberObj.toString();
                        Object photoUrlObj = oneUserDatasnapshot.child("photoUrl").getValue();
                        if(photoUrlObj==null){
                            continue;
                        }
                        String photoUrl=photoUrlObj.toString();
                        Iterable<DataSnapshot> userSellDatasnapshot = oneUserDatasnapshot.getChildren();
                        for(DataSnapshot onesellDatasnapshot : userSellDatasnapshot ){
                            if(!onesellDatasnapshot.getKey().equals("name") &&
                                    !onesellDatasnapshot.getKey().equals("number")&&
                                    !onesellDatasnapshot.getKey().equals("photoUrl")){
                                String sellId = onesellDatasnapshot.getKey();
                                Object latObj = onesellDatasnapshot.child("lat").getValue();
                                if(latObj==null){
                                    continue;
                                }
                                String latString =  latObj.toString();
                                Object lngObj = onesellDatasnapshot.child("lng").getValue();
                                if(lngObj==null){
                                    continue;
                                }
                                String lngString =  lngObj.toString();
                                double lat = Double.parseDouble(latString);
                                double lng = Double.parseDouble(lngString);
                                Object statusObj = onesellDatasnapshot.child("status").getValue();
                                if(statusObj==null){
                                    continue;
                                }
                                String status = statusObj.toString();
                                Object locationObj =onesellDatasnapshot.child("location").getValue();
                                if(locationObj==null){
                                    continue;
                                }
                                String location = locationObj.toString();
                                Object timeStampObj =onesellDatasnapshot.child("timeStamp").getValue();
                                if(timeStampObj==null){
                                    continue;
                                }
                                String timeStamp = timeStampObj.toString();
                                long timeStampp = Long.parseLong(timeStamp);
                                String timeText = Client.dateTime(timeStampp);
                                Object priceObj = onesellDatasnapshot.child("price").getValue();
                                if(priceObj==null){
                                    continue;
                                }
                                String price = priceObj.toString();
                                SellersBasicInfo sellersBasicInfo = new SellersBasicInfo(name ,number
                                        , photoUrl , lat,lng,location , status , uid , sellId , timeStamp,timeText,
                                        price);
                                sellersBasicInfoArrayList.add(sellersBasicInfo);
                            }
                        }
                    }
                    for (SellersBasicInfo sellersBasicInfo : sellersBasicInfoArrayList){
                        double lat = Double.parseDouble(userRequestDetails.getLat());
                        double lng = Double.parseDouble(userRequestDetails.getLng());
                        sellersBasicInfo.distance = distance(lat,lng , sellersBasicInfo.lat,sellersBasicInfo.lng);
                    }
                    Collections.sort(sellersBasicInfoArrayList , new SellerTimeSort());
                    sellerDetailsAdapter.notifyDataSetChanged();
                    showProgress(false);
                    notifyLinearLayout.setVisibility(View.VISIBLE);
                    if(sellersBasicInfoArrayList.size()==0){
                        noSellerLinearLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    showProgress(false);
                    Snackbar.make(findViewById(R.id.potential_seller_layout),"Try Again",
                            Snackbar.LENGTH_SHORT).show();

                }
            };
            dbReference.addListenerForSingleValueEvent(mListener);
        }

        else if(activity_code==Constants.NOTIFICATION_ACTIVITY){
            notificationAdDetails = (NotificationAdDetails) intent.getSerializableExtra("notification");
            long lastSeenTimeStamp = intent.getLongExtra("lastSeenTimeStamp", 0);
            sellerDetailsAdapter=new SellerDetailsAdapter(this,sellersBasicInfoArrayList,this,lastSeenTimeStamp);
            potentialSellerRecyclerView.setAdapter(sellerDetailsAdapter);
            potentialSellerRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
            potentialSellerRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
            potentialSellerRecyclerView.setItemAnimator(new DefaultItemAnimator());
            final DatabaseReference  dbReference = Client.getmSchoolDatabaseReference().
                    child(notificationAdDetails.getSchoolName())
                    .child(notificationAdDetails.getClassName()).child("sell");

            ValueEventListener mListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mDataSnapshot = dataSnapshot;
                    Iterable<DataSnapshot> userDatasnapshot = dataSnapshot.getChildren();

                    for(DataSnapshot oneUserDatasnapshot : userDatasnapshot ){
                        String uid = oneUserDatasnapshot.getKey();
                        Object nameObj = oneUserDatasnapshot.child("name").getValue();
                        if(nameObj==null){
                            continue;
                        }
                        String name = nameObj.toString();
                        Object numberObj = oneUserDatasnapshot.child("number").getValue();
                        if(numberObj==null){
                            continue;
                        }
                        String number = numberObj.toString();
                        Object photoUrlObj = oneUserDatasnapshot.child("photoUrl").getValue();
                        if(photoUrlObj==null){
                            continue;
                        }
                        String photoUrl=photoUrlObj.toString();
                        Iterable<DataSnapshot> userSellDatasnapshot = oneUserDatasnapshot.getChildren();
                        for(DataSnapshot onesellDatasnapshot : userSellDatasnapshot ){
                            if(!onesellDatasnapshot.getKey().equals("name") &&
                                    !onesellDatasnapshot.getKey().equals("number")&&
                                    !onesellDatasnapshot.getKey().equals("photoUrl")){
                                Object sellTimeStampObj = onesellDatasnapshot.child("timeStamp").getValue();
                                if(sellTimeStampObj==null){
                                    continue;
                                }
                                String sellTimeStamp = sellTimeStampObj.toString();
                                if(Long.parseLong(sellTimeStamp)>=Long.parseLong(notificationAdDetails.getTimeStamp())) {
                                    String sellId = onesellDatasnapshot.getKey();
                                    Object latObj = onesellDatasnapshot.child("lat").getValue();
                                    if(latObj==null){
                                        continue;
                                    }
                                    String latString =  latObj.toString();
                                    Object lngObj = onesellDatasnapshot.child("lng").getValue();
                                    if(lngObj==null){
                                        continue;
                                    }
                                    String lngString =  lngObj.toString();
                                    double lat = Double.parseDouble(latString);
                                    double lng = Double.parseDouble(lngString);
                                    Object statusObj = onesellDatasnapshot.child("status").getValue();
                                    if(statusObj==null){
                                        continue;
                                    }
                                    String status = statusObj.toString();
                                    Object locationObj =onesellDatasnapshot.child("location").getValue();
                                    if(locationObj==null){
                                        continue;
                                    }
                                    String location = locationObj.toString();
                                    long timeStampp = Long.parseLong(sellTimeStamp);
                                    String timeText = Client.dateTime(timeStampp);
                                    Object priceObj = onesellDatasnapshot.child("price").getValue();
                                    if(priceObj==null){
                                        continue;
                                    }
                                    String price = priceObj.toString();
                                    SellersBasicInfo sellersBasicInfo = new SellersBasicInfo(name, number, photoUrl,
                                            lat, lng, location, status, uid, sellId,sellTimeStamp , timeText,price);
                                    sellersBasicInfoArrayList.add(sellersBasicInfo);
                                }
                            }
                        }
                    }
                    for (SellersBasicInfo sellersBasicInfo : sellersBasicInfoArrayList){
                        double lat = Double.parseDouble(notificationAdDetails.getLat());
                        double lng = Double.parseDouble(notificationAdDetails.getLng());
                        sellersBasicInfo.distance = distance(lat,lng , sellersBasicInfo.lat,sellersBasicInfo.lng);
                    }
                    Collections.sort(sellersBasicInfoArrayList , new SellerTimeSort());
                    sellerDetailsAdapter.notifyDataSetChanged();
                    showProgress(false);
                    if(sellersBasicInfoArrayList.size()==0){
                        TextView noNewSellerTextView = findViewById(R.id.no_new_seller_textView);
                        noNewSellerTextView.setText("No New Seller Found");
                        noSellerLinearLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    showProgress(false);
                    Snackbar.make(findViewById(R.id.potential_seller_layout),"Try Again",
                            Snackbar.LENGTH_SHORT).show();

                }
            };
            dbReference.addListenerForSingleValueEvent(mListener);
        }

    }





    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return Math.abs(dist);
    }


    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }


    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    private void showProgress(boolean show){
        mProgressBar.setVisibility(show? View.VISIBLE: View.GONE);
    }

    @Override
    public void onButtonClicked(int position) {
        SellersBasicInfo sellersBasicInfo = sellersBasicInfoArrayList.get(position);
        DataSnapshot  dataSnapshot = mDataSnapshot.child(sellersBasicInfo.UID).child(sellersBasicInfo.sellID);
        Intent intent = new Intent(this,SellerBookDetailsActivity.class);
        intent.putExtra(Constants.SELLER_DETAILS , sellersBasicInfo);
        ArrayList<ShowBookDetail> arrayList = makeBooksArrayList(dataSnapshot);
        intent.putExtra(Constants.SHOW_BOOKS , (Serializable)arrayList);
        startActivity(intent);

    }

    private ArrayList<ShowBookDetail> makeBooksArrayList(DataSnapshot dataSnapshot) {
        ArrayList<ShowBookDetail> arr = new ArrayList<>();
        Iterable<DataSnapshot> bookDatasnapshot = dataSnapshot.getChildren();
        for(DataSnapshot oneBookDatasnapshot : bookDatasnapshot){
            if(!oneBookDatasnapshot.getKey().equals("lat") && !oneBookDatasnapshot.getKey().equals("lng")
                    && !oneBookDatasnapshot.getKey().equals("location")
                    && !oneBookDatasnapshot.getKey().equals("status")
                    && !oneBookDatasnapshot.getKey().equals("timeStamp")
                    && !oneBookDatasnapshot.getKey().equals("price")){

                Object bookIdObj = oneBookDatasnapshot.child("bookId").getValue();
                if(bookIdObj==null){
                    continue;
                }
                String bookId = bookIdObj.toString();
                Object bookUrlObj = oneBookDatasnapshot.child("bookUrl").getValue();
                if(bookUrlObj==null){
                    continue;
                }
                String bookUrl = bookUrlObj.toString();
                Object conditionObj = oneBookDatasnapshot.child("condition").getValue();
                if(conditionObj==null){
                    continue;
                }
                String condition = conditionObj.toString();
                Object descriptionObj = oneBookDatasnapshot.child("description").getValue();
                if(descriptionObj==null){
                    continue;
                }
                String description = descriptionObj.toString();
                Object titleObj = oneBookDatasnapshot.child("title").getValue();
                if(titleObj==null){
                    continue;
                }
                String title = titleObj.toString();
                ShowBookDetail showBookDetail = new ShowBookDetail(bookId , bookUrl , condition , description , title);
                arr.add(showBookDetail);
            }
        }
        return arr;
    }

    @Override
    public void onClick(View view) {
        if(isBellClicked){
            return;
        }
        isBellClicked=true;
        SQLiteDatabase database = DatabaseClient.getWritableDatabase(this);
        String selectionArgs[]={DatabaseConstants.BUY_CATEGORY};
        Cursor cursor = database.query(OpenHelper.USER_SELLBUY_TABLE_NAME,null,OpenHelper.CATEGORY+" = ?",
                selectionArgs,null,null,null);
        int count=0;
        while (cursor.moveToNext()){
            String schoolName = cursor.getString(cursor.getColumnIndex(OpenHelper.SCHOOL));
            String className = cursor.getString(cursor.getColumnIndex(OpenHelper.CLASS));
            if(schoolName.equals(userRequestDetails.getSchoolName()) &&
                    className.equals(userRequestDetails.getClassName())){

                Snackbar.make(findViewById(R.id.potential_seller_layout),"Go to profile to Unnotify",
                        Snackbar.LENGTH_LONG).setAction("Profile", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name;
                        if(User.getName()==null){
                            name = "";
                        }else{
                            name = User.getName();
                        }
                        Intent intent = new Intent(PotentialSellersActivity.this , ProfileActivity.class);
                        intent.putExtra("name" , name);
                        startActivity(intent);
                    }
                }).show();
                isBellClicked=false;
                return;
            }
            count++;
        }
        cursor.close();


        if(count>=5){
            View mainView = findViewById(R.id.potential_seller_layout);
            Snackbar.make(mainView , "You cannot Post more than 5 buy Ads" , Snackbar.LENGTH_LONG).show();
            isBellClicked=false;
            return;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(PotentialSellersActivity.this);
        builder.setCancelable(false);
        builder.setTitle("Post Buy Request");
        builder.setMessage("Posting your buy request will enable sellers to contact you if they found you as a potential buyer." +
                "You may receive phone call");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if(!Client.haveNetworkConnection(PotentialSellersActivity.this)){
                    View mainView = findViewById(R.id.potential_seller_layout);
                    Snackbar.make(mainView , "No Internet Connection" , Snackbar.LENGTH_LONG).show();
                    isBellClicked=false;
                    return;
                }

                progressDialog.show();

                DatabaseReference reference =  Client.getmUserDatabaseReference().child(Client.getmFirebaseUser().getUid()).child("buy");
                final String buyId =reference.push().getKey();
                String timeStamp = String.valueOf(System.currentTimeMillis());

                SQLiteDatabase database  = DatabaseClient.getWritableDatabase(PotentialSellersActivity.this);
                ContentValues cv = new ContentValues();
                cv.put(OpenHelper.SCHOOL,userRequestDetails.getSchoolName());
                cv.put(OpenHelper.CLASS,userRequestDetails.getClassName());
                cv.put(OpenHelper.LOCATION,userRequestDetails.getLocation());
                cv.put(OpenHelper.LATITUDE,userRequestDetails.getLat());
                cv.put(OpenHelper.LONGITUDE,userRequestDetails.getLng());
                cv.put(OpenHelper.SELLBUY_ID,buyId);
                cv.put(OpenHelper.CATEGORY, DatabaseConstants.BUY_CATEGORY);
                cv.put(OpenHelper.STATUS,"");
                cv.put(OpenHelper.TIME_STAMP , timeStamp);
                cv.put(OpenHelper.LAST_SEEN_TIME_STAMP , timeStamp);
                database.insert(OpenHelper.USER_SELLBUY_TABLE_NAME,null,cv);

                HashMap<String,Object> user = new HashMap<String, Object>();
                user.put("name" , User.getName());
                user.put("number" , User.getNumber());
                user.put("photoUrl" , User.getPhotoURL());
                HashMap<String,Object> locationdetails = new HashMap<String, Object>();
                locationdetails.put("lat" , userRequestDetails.getLat());
                locationdetails.put("lng" , userRequestDetails.getLng());
                locationdetails.put("location",userRequestDetails.getLocation());
                locationdetails.put("timeStamp" , timeStamp);
                user.put(buyId , locationdetails);

                HashMap<String ,String > userAdDetails = new HashMap<String, String>();
                userAdDetails.put("className" , userRequestDetails.getClassName());
                userAdDetails.put("schoolName" , userRequestDetails.getSchoolName());
                userAdDetails.put("lat" , userRequestDetails.getLat());
                userAdDetails.put("lng" , userRequestDetails.getLng());
                userAdDetails.put("location" , userRequestDetails.getLocation());
                userAdDetails.put("timeStamp" , timeStamp);

                HashMap<String,Object> childupdates = new HashMap<String, Object>();
                childupdates.put("/users/"+Client.getmFirebaseUser().getUid()+"/buy/"+buyId  , userAdDetails );
                childupdates.put("/schools/India/New Delhi/" + userRequestDetails.getSchoolName()
                        +"/"+userRequestDetails.getClassName()+"/buy/" + Client.getmFirebaseUser().getUid() , user);
                FirebaseDatabase.getInstance().getReference().updateChildren(childupdates,
                        new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if(databaseError==null){
                                    if(progressDialog!=null && progressDialog.isShowing()){
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(PotentialSellersActivity.this, "Buy Request Posted", Toast.LENGTH_SHORT).show();
                                    isBellClicked = false;

                                }else{
                                    SQLiteDatabase database = DatabaseClient.getWritableDatabase(PotentialSellersActivity.this);
                                    String selectionArgs[] = {buyId};
                                    database.delete(OpenHelper.USER_SELLBUY_TABLE_NAME , OpenHelper.SELLBUY_ID + " = ? " , selectionArgs);
                                    if(progressDialog!=null && progressDialog.isShowing()){
                                        progressDialog.dismiss();
                                    }
                                    Toast.makeText(PotentialSellersActivity.this, "Buy Request Not Posted.Try Again", Toast.LENGTH_SHORT).show();
                                    isBellClicked = false;
                                }
                            }
                        });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                isBellClicked=false;
            }
        });
        builder.create().show();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.sort_distance){
            Collections.sort(sellersBasicInfoArrayList , new SellerDistanceSort());
            sellerDetailsAdapter.notifyDataSetChanged();

        }else if(item.getItemId() == R.id.sort_date){
            Collections.sort(sellersBasicInfoArrayList , new SellerTimeSort());
            sellerDetailsAdapter.notifyDataSetChanged();
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(activity_code==Constants.NOTIFICATION_ACTIVITY){
            startActivity(new Intent(this , NotificationActivity.class));
            finish();
        }
        else if(activity_code==Constants.GENERAL_ACTIVITY){
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
