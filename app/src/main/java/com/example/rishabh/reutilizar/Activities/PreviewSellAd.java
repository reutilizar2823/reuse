package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.rishabh.reutilizar.Adapters.SellAdAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.util.ArrayList;

public class PreviewSellAd extends AppCompatActivity implements SellAdAdapter.NotifyInterface, SellAdAdapter.BookViewClickListener {

    private RecyclerView sellAdRecyclerView;
    private String schoolName,className;
    private int activity_code;
    private UserRequestDetails userRequestDetails;
    private SellAdAdapter adapter;
    private ArrayList<OneBookDetail> booksDetailArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_sell_ad);
        setTitle("Books Details");
        Intent intent=getIntent();
        activity_code = intent.getIntExtra("partialActivity" ,0);
        userRequestDetails = (UserRequestDetails) intent.getSerializableExtra("userDetails");

        final String sellId=intent.getStringExtra("sellId");
        Log.d("hola" , sellId);
        schoolName=userRequestDetails.getSchoolName();
        className = userRequestDetails.getClassName();
        sellAdRecyclerView=findViewById(R.id.sell_ad_recyclerView);
        //Async Task
        new AsyncTask<Void, Void, ArrayList<OneBookDetail>>() {

            @Override
            protected ArrayList<OneBookDetail> doInBackground(Void... voids) {
                 booksDetailArrayList = new ArrayList<OneBookDetail>();
                fetchDataFromSQLite(sellId,booksDetailArrayList);
                return booksDetailArrayList;
            }

            @Override
            protected void onPostExecute(final ArrayList<OneBookDetail> oneBookDetailArrayList) {
                super.onPostExecute(oneBookDetailArrayList);
                if(oneBookDetailArrayList.size()==0){
                    if(!Client.haveNetworkConnection(PreviewSellAd.this)){
                        View mainLayout = findViewById(R.id.preview_sellAd_mainLayout);
                        Snackbar.make(mainLayout , "No Internet Connection.Books Details were not restored due to signout"
                                , Snackbar.LENGTH_SHORT ).show();
                        return;
                    }
                    final ProgressDialog mDialog = new ProgressDialog(PreviewSellAd.this);
                    mDialog.setMessage("Restoring Books Details");
                    mDialog.setCancelable(false);
                    mDialog.show();
                    DatabaseReference sellAdReference = Client.getmSchoolDatabaseReference().child(schoolName)
                                                        .child(className).child("sell")
                                                         .child(Client.getmFirebaseUser().getUid())
                                                          .child(sellId);

                    ValueEventListener mListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            for(DataSnapshot  oneBookDatasnapshot: dataSnapshot.getChildren()){
                                if(!oneBookDatasnapshot.getKey().equals("lat") && !oneBookDatasnapshot.getKey().equals("lng")
                                        && !oneBookDatasnapshot.getKey().equals("location")
                                        && !oneBookDatasnapshot.getKey().equals("status")
                                        && !oneBookDatasnapshot.getKey().equals("timeStamp")
                                        && !oneBookDatasnapshot.getKey().equals("price")){

                                    Object bookIdObj = oneBookDatasnapshot.child("bookId").getValue();
                                    if(bookIdObj==null){
                                        continue;
                                    }

                                    String bookId = bookIdObj.toString();
                                    Object bookUrlObj = oneBookDatasnapshot.child("bookUrl").getValue();
                                    if(bookUrlObj==null){
                                        continue;
                                    }

                                    String bookUrl = bookUrlObj.toString();
                                    Object conditionObj = oneBookDatasnapshot.child("condition").getValue();
                                    if(conditionObj==null){
                                        continue;
                                    }

                                    String condition = conditionObj.toString();
                                    Object descriptionObj = oneBookDatasnapshot.child("description").getValue();
                                    if(descriptionObj==null){
                                        continue;
                                    }

                                    String description = descriptionObj.toString();
                                    Object titleObj = oneBookDatasnapshot.child("title").getValue();
                                    if(titleObj==null){
                                        continue;
                                    }

                                    String title = titleObj.toString();
                                    OneBookDetail oneBookDetail = new OneBookDetail(title , condition , "" , description , bookUrl , bookId);
                                    oneBookDetailArrayList.add(oneBookDetail);
                                }
                            }
                            insertIntoLocalDatabase(oneBookDetailArrayList,sellId);
                            if(mDialog!=null && mDialog.isShowing()){
                                mDialog.dismiss();
                            }
                            adapter = new SellAdAdapter(PreviewSellAd.this, oneBookDetailArrayList,
                                    sellId, schoolName, className, PreviewSellAd.this, PreviewSellAd.this);
                            sellAdRecyclerView.setAdapter(adapter);
                            sellAdRecyclerView.setLayoutManager(new LinearLayoutManager(PreviewSellAd.this, LinearLayoutManager.VERTICAL, false));
                            sellAdRecyclerView.addItemDecoration(new DividerItemDecoration(PreviewSellAd.this, DividerItemDecoration.VERTICAL));
                            sellAdRecyclerView.setItemAnimator(new DefaultItemAnimator());

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            if(mDialog!=null && mDialog.isShowing()){
                                mDialog.dismiss();
                            }

                        }
                    };
                    sellAdReference.addListenerForSingleValueEvent(mListener);
                }
                else {
                    adapter = new SellAdAdapter(PreviewSellAd.this, oneBookDetailArrayList,
                            sellId, schoolName, className, PreviewSellAd.this, PreviewSellAd.this);
                    sellAdRecyclerView.setAdapter(adapter);
                    sellAdRecyclerView.setLayoutManager(new LinearLayoutManager(PreviewSellAd.this, LinearLayoutManager.VERTICAL, false));
                    sellAdRecyclerView.addItemDecoration(new DividerItemDecoration(PreviewSellAd.this, DividerItemDecoration.VERTICAL));
                    sellAdRecyclerView.setItemAnimator(new DefaultItemAnimator());
                }

            }
        }.execute();


    }

    private void insertIntoLocalDatabase(ArrayList<OneBookDetail> arrayList , String sellId) {
        SQLiteDatabase db = DatabaseClient.getWritableDatabase(this);
        for(OneBookDetail oneBookDetail : arrayList) {
            ContentValues cv = new ContentValues();
            cv.put(OpenHelper.TITLE , oneBookDetail.title);
            cv.put(OpenHelper.CONDITION , oneBookDetail.condition);
            cv.put(OpenHelper.DESCRIPTION , oneBookDetail.description);
            cv.put(OpenHelper.BOOK_ID , oneBookDetail.bookId);
            cv.put(OpenHelper.IMAGE_URL , oneBookDetail.bookUrl);
            cv.put(OpenHelper.IMAGE_PATH , "");
            cv.put(OpenHelper.SELL_ID , sellId);
            db.insert(OpenHelper.BOOK_DETAILS_TABLE_NAME , null , cv);
        }
    }

    private void fetchDataFromSQLite(String sellId,ArrayList<OneBookDetail> booksDetailArrayList) {
        SQLiteDatabase database= DatabaseClient.getReadableDatabase(this);
        String[] selections = {sellId};
        Cursor cursor=database.query(OpenHelper.BOOK_DETAILS_TABLE_NAME,null,OpenHelper.SELL_ID+" = ? ",selections,null,null,null);
        while(cursor.moveToNext()){
            String title = cursor.getString(cursor.getColumnIndex(OpenHelper.TITLE));
            String condition = cursor.getString(cursor.getColumnIndex(OpenHelper.CONDITION));
            String description = cursor.getString(cursor.getColumnIndex(OpenHelper.DESCRIPTION));
            String imagePath = cursor.getString(cursor.getColumnIndex(OpenHelper.IMAGE_PATH));
            String imageUrl = cursor.getString(cursor.getColumnIndex(OpenHelper.IMAGE_URL));
            String bookId = cursor.getString(cursor.getColumnIndex(OpenHelper.BOOK_ID));
            OneBookDetail oneBookDetail = new OneBookDetail(title,condition,imagePath,description,imageUrl,bookId);
            booksDetailArrayList.add(oneBookDetail);
        }
        cursor.close();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.potential_buyer_menu , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.potential_buyer_menu){
            Intent intent = new Intent(this , PotentialBuyerActivity.class);
            intent.putExtra(Constants.SELLER_DETAILS , userRequestDetails);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(activity_code==Constants.PARTIAL_BOOK_ACTIVITY) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void imageUploaded(int position) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBookImageClicked(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.image_popup,null);
        builder.setView(view);
        ImageView imageView = view.findViewById(R.id.popup_imageView);

        if(booksDetailArrayList.get(position).bookUrl.equals("")){
            imageView.setImageResource(R.drawable.default_book);
        }else {
            Glide.with(this).load(booksDetailArrayList.get(position).bookUrl)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            //Set Error Image
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(imageView);
        }
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    public void onDescriptionClicked(int position) {
        String desc = booksDetailArrayList.get(position).description;
        if(desc.equals("")){
            desc="No Description";
        }
            /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Description of " + showBookDetailArrayList.get(position).title);
            builder.setMessage(desc);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                  dialogInterface.dismiss();
                }
            });
            builder.create().show();*/
            new LovelyInfoDialog(this)
                    .setIcon(R.drawable.description_icon)
                    .setTopColorRes(R.color.colorPrimary)
                    .setTitle(booksDetailArrayList.get(position).title)
                    .setMessage("Description : "+desc)
                    .show();


    }
}
