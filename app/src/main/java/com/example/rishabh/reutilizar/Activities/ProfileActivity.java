package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;


import com.example.rishabh.reutilizar.Adapters.ProfileSectionPagerAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.firebase.auth.FirebaseAuth;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

public class ProfileActivity extends AppCompatActivity {
    private String name,phoneNo;
    private ProfileSectionPagerAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView nameNumberTextView;
    private MaterialLetterIcon materialLetterIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("My Account");

        nameNumberTextView = findViewById(R.id.profile_name_number_textView);
        materialLetterIcon = findViewById(R.id.material_letter_icon);
        Intent intent = getIntent();
        name=intent.getStringExtra("name");
        phoneNo = Client.getmFirebaseUser().getPhoneNumber();
        materialLetterIcon.setLetter(name.charAt(0)+"");
        nameNumberTextView.setText(name+"\n"+phoneNo);
        adapter=new ProfileSectionPagerAdapter(getSupportFragmentManager());
        viewPager=findViewById(R.id.profile_container);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        tabLayout=findViewById(R.id.profile_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu , menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.sign_out_menu){
            if(!Client.haveNetworkConnection(this)){
                Snackbar.make(findViewById(R.id.profile_activity_main_view)  , "No Internet Connection " , Snackbar.LENGTH_SHORT).show();
                return true;
            }
              signOut();
        }else if(item.getItemId()==R.id.my_uid_menu){
            new LovelyInfoDialog(this)
                    .setIcon(R.drawable.uid)
                    .setTopColorRes(R.color.colorPrimary)
                    .setTitle("UID Number")
                    .setMessage(Client.getmFirebaseUser().getUid())
                    .show();
        }
        return  true;
    }

    private void signOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ProgressDialog progress = new ProgressDialog(ProfileActivity.this);
                progress.setMessage("Signing you out...");
                progress.show();
                SharedPreferences sharedPreferences =getSharedPreferences(Constants.SHARED_PREFERENCE_NAME,MODE_PRIVATE);
                sharedPreferences.edit().clear().apply();
                SQLiteDatabase  db = DatabaseClient.getWritableDatabase(ProfileActivity.this);
               // db.delete(OpenHelper.USER_TABLE_NAME,null,null);
                db.delete(OpenHelper.USER_SELLBUY_TABLE_NAME,null,null);
                db.delete(OpenHelper.BOOK_DETAILS_TABLE_NAME,null,null);
                Client.signOut();
                FirebaseAuth.getInstance().signOut();
                if(progress!=null && progress.isShowing())
                    progress.dismiss();
                Intent intent = new Intent(ProfileActivity.this , MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }
}
