package com.example.rishabh.reutilizar.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.rishabh.reutilizar.Adapters.ShowBooksDetailsAdapter;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Models.SellersBasicInfo;
import com.example.rishabh.reutilizar.Models.ShowBookDetail;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.util.ArrayList;

public class SellerBookDetailsActivity extends AppCompatActivity implements ShowBooksDetailsAdapter.BookViewClickListener {
    private ArrayList<ShowBookDetail> showBookDetailArrayList;
    private SellersBasicInfo sellersBasicInfo;
    private RecyclerView mRecyclerView;
    private ShowBooksDetailsAdapter mAdapter;
    private TextView nameTextView,addressTextView;
    private ImageView phoneImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_book_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        showBookDetailArrayList= (ArrayList<ShowBookDetail>) intent.getSerializableExtra(Constants.SHOW_BOOKS);
        sellersBasicInfo = (SellersBasicInfo)intent.getSerializableExtra(Constants.SELLER_DETAILS);
        nameTextView = findViewById(R.id.seller_name_textView);
        addressTextView = findViewById(R.id.seller_address_textView);
        phoneImageView = findViewById(R.id.seller_phone_imageView);
        nameTextView.setText(sellersBasicInfo.name);
        addressTextView.setText(sellersBasicInfo.location);
        phoneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
                phoneIntent.setData(Uri.parse("tel:"+sellersBasicInfo.number));
                startActivity(phoneIntent);
            }
        });
        mRecyclerView = findViewById(R.id.show_books_recyclerView);
        mAdapter=new ShowBooksDetailsAdapter(this,showBookDetailArrayList,this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void onBookImageClicked(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.image_popup,null);
        builder.setView(view);
        ImageView imageView = view.findViewById(R.id.popup_imageView);

        if(showBookDetailArrayList.get(position).bookUrl.equals("")){
            imageView.setImageResource(R.drawable.default_book);
        }else {
            Glide.with(this).load(showBookDetailArrayList.get(position).bookUrl)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            //Set Error Image
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).into(imageView);
        }
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    @Override
    public void onDescriptionClicked(int position) {
        String desc = showBookDetailArrayList.get(position).description;
        if(desc.equals("")){
            desc="No Description";
        }
            /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Description of " + showBookDetailArrayList.get(position).title);
            builder.setMessage(desc);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                  dialogInterface.dismiss();
                }
            });
            builder.create().show();*/
            new LovelyInfoDialog(this)
                    .setIcon(R.drawable.description_icon)
                    .setTopColorRes(R.color.colorPrimary)
                    .setTitle(showBookDetailArrayList.get(position).title)
                    .setMessage("Description : "+desc)
                    .show();

    }
}
