package com.example.rishabh.reutilizar.Activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rishabh.reutilizar.Activities.HomeActivity;
import com.example.rishabh.reutilizar.Activities.MainActivity;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.DatabaseConstants;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.User;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class UserDetailActivity extends AppCompatActivity {

    private EditText name_EditText;
    private String name ;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);

        name_EditText = (EditText) findViewById(R.id.name_editText);
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Logging you in....");
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this , MainActivity.class));
        finish();
    }

    public void SubmitName(View view) {
      if(!Client.haveNetworkConnection(this)){
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return ;
        }

        if(name_EditText.getText()==null){
            name_EditText.setError("Can't be empty");
            return;
        }
        name = name_EditText.getText().toString();
        if(name.trim().isEmpty()){
            name_EditText.setError("Can't be empty");
            return;
        }
        mDialog.setCancelable(false);
        mDialog.show();
        DatabaseReference userNodeRef = Client.getmUserDatabaseReference().child(Client.getmFirebaseUser().getUid());
        ValueEventListener mListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot==null){
                    insertUserPersonalDetails();
                }else{
                   // mDialog.setMessage("Restoring data");
                    backUpUserData(dataSnapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        userNodeRef.addListenerForSingleValueEvent(mListener);

    }

    private void insertUserPersonalDetails() {
        String phoneNumber = Client.getmFirebaseUser().getPhoneNumber();
        String UID = Client.getmFirebaseUser().getUid();
        insertFirebaseDatabase(name,phoneNumber,UID,"");
        SharedPreferences sharedPreferences=Client.getSharedPreferences(UserDetailActivity.this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.NAME , name);
        editor.commit();
        //  insertDatabase(name , phoneNumber , UID , "");
        if(mDialog!=null && mDialog.isShowing()){
            mDialog.dismiss();
        }
        startActivity(new Intent(UserDetailActivity.this , HomeActivity.class));
        finish();
    }

    private void backUpUserData(DataSnapshot dataSnapshot) {
        SQLiteDatabase  db = DatabaseClient.getWritableDatabase(this);
        db.delete(OpenHelper.USER_SELLBUY_TABLE_NAME,null,null);
        DataSnapshot buyAdsDataSnapshot = dataSnapshot.child("buy");
        for(DataSnapshot onebuyAdDataSnapshot : buyAdsDataSnapshot.getChildren()){
            String buyId = onebuyAdDataSnapshot.getKey();
            Object classNameObj = onebuyAdDataSnapshot.child("className").getValue();
            if(classNameObj==null){
                continue;
            }
            String className = classNameObj.toString();
            Object schoolNameObj = onebuyAdDataSnapshot.child("schoolName").getValue();
            if(schoolNameObj==null){
                continue;
            }
            String schoolName = schoolNameObj.toString();
            Object latObj = onebuyAdDataSnapshot.child("lat").getValue();
            if(latObj==null){
                continue;
            }
            String latString = latObj.toString();
            Object lngObj  = onebuyAdDataSnapshot.child("lng").getValue();
            if(lngObj==null){
                continue;
            }
            String lngString  = lngObj.toString();
            Object locationObj = onebuyAdDataSnapshot.child("location").getValue();
            if(locationObj==null){
                continue;
            }
            String location = locationObj.toString();
            Object timeStampObj = onebuyAdDataSnapshot.child("timeStamp").getValue();
            if(timeStampObj==null){
                continue;
            }
            String timeStamp = timeStampObj.toString();
            ContentValues cv = new ContentValues();
            cv.put(OpenHelper.SCHOOL , schoolName);
            cv.put(OpenHelper.CLASS , className);
            cv.put(OpenHelper.LATITUDE , latString);
            cv.put(OpenHelper.LONGITUDE , lngString);
            cv.put(OpenHelper.LOCATION , location);
            cv.put(OpenHelper.TIME_STAMP , timeStamp);
            cv.put(OpenHelper.LAST_SEEN_TIME_STAMP , timeStamp);
            cv.put(OpenHelper.SELLBUY_ID , buyId);
            cv.put(OpenHelper.CATEGORY , DatabaseConstants.BUY_CATEGORY);
            cv.put(OpenHelper.STATUS , "");
            cv.put(OpenHelper.PRICE_BOOK_SET , "");
            db.insert(OpenHelper.USER_SELLBUY_TABLE_NAME , null , cv);
        }
        DataSnapshot sellAdsDataSnapshot = dataSnapshot.child("sell");
        for(DataSnapshot onesellAdDataSnapshot : sellAdsDataSnapshot.getChildren()){
            String sellId = onesellAdDataSnapshot.getKey();
            Object classNameObj = onesellAdDataSnapshot.child("className").getValue();
            if(classNameObj==null){
                continue;
            }
            String className = classNameObj.toString();
            Object schoolNameObj = onesellAdDataSnapshot.child("schoolName").getValue();
            if(schoolNameObj==null){
                continue;
            }
            String schoolName = schoolNameObj.toString();
            Object latObj = onesellAdDataSnapshot.child("lat").getValue();
            if(latObj==null){
                continue;
            }
            String latString = latObj.toString();
            Object lngObj  = onesellAdDataSnapshot.child("lng").getValue();
            if(lngObj==null){
                continue;
            }
            String lngString  = lngObj.toString();
            Object locationObj = onesellAdDataSnapshot.child("location").getValue();
            if(locationObj==null){
                continue;
            }
            String location = locationObj.toString();
            Object timeStampObj = onesellAdDataSnapshot.child("timeStamp").getValue();
            if(timeStampObj==null){
                continue;
            }
            String timeStamp = timeStampObj.toString();
            Object priceObj = onesellAdDataSnapshot.child("price").getValue();
            if(priceObj==null){
                continue;
            }
            String price = priceObj.toString();
            Object statusObj = onesellAdDataSnapshot.child("status").getValue();
            if(statusObj==null){
                continue;
            }
            String status = statusObj.toString();
            ContentValues cv = new ContentValues();
            cv.put(OpenHelper.SCHOOL , schoolName);
            cv.put(OpenHelper.CLASS , className);
            cv.put(OpenHelper.LATITUDE , latString);
            cv.put(OpenHelper.LONGITUDE , lngString);
            cv.put(OpenHelper.LOCATION , location);
            cv.put(OpenHelper.TIME_STAMP , timeStamp);
            cv.put(OpenHelper.LAST_SEEN_TIME_STAMP , timeStamp);
            cv.put(OpenHelper.SELLBUY_ID , sellId);
            cv.put(OpenHelper.CATEGORY , DatabaseConstants.SELL_CATEGORY);
            cv.put(OpenHelper.STATUS , status);
            cv.put(OpenHelper.PRICE_BOOK_SET , price);
            db.insert(OpenHelper.USER_SELLBUY_TABLE_NAME , null , cv);
        }
        insertUserPersonalDetails();
    }

    private void insertFirebaseDatabase(String name, String phoneNumber, String uid , String photoUrl) {
        DatabaseReference mUserDatabaseReference = Client.getmUserDatabaseReference().child(uid);
        User.setName(name);
        User.setNumber(phoneNumber);
        User.setUID(uid);
        User.setPhotoURL(photoUrl);
        mUserDatabaseReference.child("name").setValue(name);
        mUserDatabaseReference.child("number").setValue(phoneNumber);
        mUserDatabaseReference.child("photoUrl").setValue(photoUrl);
        mUserDatabaseReference.child("uid").setValue(uid);
    }

   /* private void insertDatabase(String name, String phoneNumber, String uid , String photoUrl) {
        SQLiteDatabase database = DatabaseClient.getWritableDatabase(this);
        ContentValues cv = new ContentValues();
        cv.put(OpenHelper.USER_NAME,name);
        cv.put(OpenHelper.USER_NUMBER,phoneNumber);
        cv.put(OpenHelper.USER_UID,uid);
        cv.put(OpenHelper.USER_PHOTO_URL , photoUrl);
        database.insert(OpenHelper.USER_TABLE_NAME,null,cv);
    }*/
}
