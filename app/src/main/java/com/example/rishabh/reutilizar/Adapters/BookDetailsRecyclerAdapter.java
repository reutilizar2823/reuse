package com.example.rishabh.reutilizar.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 9/5/17.
 */

public class BookDetailsRecyclerAdapter extends RecyclerView.Adapter<BookDetailsRecyclerAdapter.BookViewHolder> {
     ArrayList<OneBookDetail> oneBookDetailArrayList;
    Context context;
    BookViewClickListener mListener;

    public BookDetailsRecyclerAdapter(Context context,ArrayList<OneBookDetail> oneBookDetailArrayList
            ,BookViewClickListener mListener){
        this.context=context;
        this.oneBookDetailArrayList=oneBookDetailArrayList;
        this.mListener=mListener;
    }


    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.one_book_detail_view,parent,false);
        return new BookViewHolder(itemView,oneBookDetailArrayList,mListener,context);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        holder.titleTextView.setText(oneBookDetailArrayList.get(position).title);

        holder.bookSelectedCheckBox.setChecked(oneBookDetailArrayList.get(position).checkBox);
        if(oneBookDetailArrayList.get(position).condition.equals(Constants.getBestBookCondition())){
            holder.conditionRadioGroup.check(R.id.best_book_condition_radio_button);
        }else if(oneBookDetailArrayList.get(position).condition.equals(Constants.getMediumBookCondition())){
            holder.conditionRadioGroup.check(R.id.medium_book_condition_radio_button);
        }else{
            holder.conditionRadioGroup.check(R.id.worst_book_condition_radio_button);
        }
    }

    @Override
    public int getItemCount() {
        return oneBookDetailArrayList.size();
    }

    public static class BookViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener, RadioGroup.OnCheckedChangeListener, View.OnClickListener, View.OnLongClickListener {
        TextView titleTextView;
        RadioGroup conditionRadioGroup;
        CheckBox bookSelectedCheckBox;
        Button moreInfoButton;
        BookViewClickListener mListener;
        ArrayList<OneBookDetail> oneBookDetailArrayList;
        Context context;

        public BookViewHolder(View itemView,ArrayList<OneBookDetail> oneBookDetailArrayList,BookViewClickListener mListener,
                              Context context) {
            super(itemView);
            this.mListener=mListener;
            this.oneBookDetailArrayList=oneBookDetailArrayList;
            this.context=context;
            titleTextView=itemView.findViewById(R.id.book_title_textView);
            conditionRadioGroup=itemView.findViewById(R.id.condition_book_radioGroup);
            bookSelectedCheckBox=itemView.findViewById(R.id.book_selected_checkBox);
            moreInfoButton=itemView.findViewById(R.id.more_info_book_button);
            bookSelectedCheckBox.setOnCheckedChangeListener(this);
            conditionRadioGroup.setOnCheckedChangeListener(this);
            moreInfoButton.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            for(int i=0;i<conditionRadioGroup.getChildCount();i++){
                ((RadioButton)conditionRadioGroup.getChildAt(i)).setEnabled(b);
            }
            int position = getAdapterPosition();
            oneBookDetailArrayList.get(position).checkBox=b;
            moreInfoButton.setEnabled(b);
            if(b){
                moreInfoButton.setBackground(context.getResources().getDrawable(R.drawable.button_background));
            }else{
                moreInfoButton.setBackground(context.getResources().getDrawable(R.drawable.light_button_background));
            }

            }

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
            int position = getAdapterPosition();
            if(i==R.id.best_book_condition_radio_button){
                oneBookDetailArrayList.get(position).condition=Constants.getBestBookCondition();
            }else if(i==R.id.medium_book_condition_radio_button){
                oneBookDetailArrayList.get(position).condition=Constants.getMediumBookCondition();
            }else{
                oneBookDetailArrayList.get(position).condition=Constants.getWorstBookCondition();
            }
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onMoreInfoButtonClicked(view,position);

        }

        @Override
        public boolean onLongClick(View view) {

            int position=getAdapterPosition();
            if(!oneBookDetailArrayList.get(position).owner){
                mListener.onLongClicked(position);
            }
            return true;
        }
    }
    public interface BookViewClickListener{
        void onMoreInfoButtonClicked(View view,int position);
        void onLongClicked(int position);

    }
    }

