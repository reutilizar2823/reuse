package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rishabh.reutilizar.Models.BuyerBasicInfo;
import com.example.rishabh.reutilizar.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by rishabh on 9/10/17.
 */

public class BuyerDetailsAdapter extends RecyclerView.Adapter<BuyerDetailsAdapter.BuyerViewHolder> {

    Context context;
    ArrayList<BuyerBasicInfo> buyerBasicInfoArrayList;
    BuyerDetailsClickListener mListener;


    public BuyerDetailsAdapter(Context context,ArrayList<BuyerBasicInfo> buyerBasicInfoArrayList,
                               BuyerDetailsClickListener mListener){
        this.context=context;
        this.buyerBasicInfoArrayList=buyerBasicInfoArrayList;
        this.mListener=mListener;
    }

    @Override
    public BuyerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.buyer_view,parent,false);

        return new BuyerViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(BuyerViewHolder holder, int position) {
        holder.nameTextView.setText(buyerBasicInfoArrayList.get(position).name);
        holder.locationTextView.setText(buyerBasicInfoArrayList.get(position).location);
        double distance = buyerBasicInfoArrayList.get(position).distance;
        DecimalFormat df = new DecimalFormat("0.0");
        holder.distanceTextView.setText(df.format(distance)+" KM");
        holder.dateTimeTextView.setText(buyerBasicInfoArrayList.get(position).actualTime);
    }

    @Override
    public int getItemCount() {
        return buyerBasicInfoArrayList.size();
    }

    public class BuyerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView distanceTextView ;
            TextView nameTextView ;
            TextView locationTextView;
            ImageView phoneImageView;
            TextView dateTimeTextView;
            BuyerDetailsClickListener mListener;

            public BuyerViewHolder(View itemView,BuyerDetailsClickListener mListener) {
                super(itemView);
                this.mListener=mListener;
                distanceTextView = itemView.findViewById(R.id.buyer_view_distance);
                nameTextView = itemView.findViewById(R.id.buyer_view_name);
                dateTimeTextView = itemView.findViewById(R.id.buyer_view_dateTime);
                locationTextView = itemView.findViewById(R.id.buyer_view_location);
                phoneImageView = itemView.findViewById(R.id.buyer_view_number);
                phoneImageView.setOnClickListener(this);
                itemView.setOnClickListener(this);
            }

        @Override
        public void onClick(View view) {
            int pos= getAdapterPosition();
            if(view.getId()==R.id.buyer_view_number)
                mListener.onPhoneClicked(pos);
            else
                mListener.onItemViewClicked(pos);
        }
    }
        public interface BuyerDetailsClickListener{
            void onItemViewClicked(int position);
            void onPhoneClicked(int position);
        }
}
