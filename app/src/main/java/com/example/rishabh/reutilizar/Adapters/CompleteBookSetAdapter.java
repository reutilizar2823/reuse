package com.example.rishabh.reutilizar.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.rishabh.reutilizar.Fragment.BuyFragment;
import com.example.rishabh.reutilizar.Fragment.SellFragment;

/**
 * Created by rishabh on 9/5/17.
 */

public class CompleteBookSetAdapter extends FragmentPagerAdapter {
    public CompleteBookSetAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0) {
            BuyFragment fragment = new BuyFragment();
            return fragment;
        }else if(position==1){
            BuyFragment fragment = new BuyFragment();
            return fragment;
        }else if(position ==2){
            BuyFragment fragment = new BuyFragment();
            return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0){
            return "1";
        }else if(position==1){
            return "2";
        }else if(position == 2){
            return "3";
        }
        return super.getPageTitle(position);
    }
}
