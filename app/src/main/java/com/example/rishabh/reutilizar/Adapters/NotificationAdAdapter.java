package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rishabh.reutilizar.Models.NotificationAdDetails;
import com.example.rishabh.reutilizar.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/18/17.
 */

public class NotificationAdAdapter  extends RecyclerView.Adapter<NotificationAdAdapter.NotificationViewHolder>{
    private Context context ;
    private ArrayList<NotificationAdDetails> arrayList;
    private onNotificationClicked listener;
    public NotificationAdAdapter(Context context , ArrayList<NotificationAdDetails> arrayList , onNotificationClicked listener){
        this.context= context;
        this.arrayList=arrayList;
        this.listener = listener;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.notification_ad_view , parent , false);
        return new NotificationViewHolder(itemView , listener);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        holder.schoolNameTextView.setText(arrayList.get(position).getSchoolName());
        holder.classNameTextView.setText(arrayList.get(position).getClassName()+" Class");
        holder.locationTextView.setText(arrayList.get(position).getLocation());

        if(arrayList.get(position).getCount()>0){
            holder.countNotificationTextView.setVisibility(View.VISIBLE);
            holder.countNotificationTextView.setText(arrayList.get(position).getCount()+"");
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {
        TextView schoolNameTextView;
        TextView classNameTextView;
        TextView locationTextView;
        TextView countNotificationTextView;
        onNotificationClicked listener;
        public NotificationViewHolder(View itemView, final onNotificationClicked listener) {
            super(itemView);
            schoolNameTextView = itemView.findViewById(R.id.notification_school_name);
            classNameTextView = itemView.findViewById(R.id.notification_class_name);
            locationTextView = itemView.findViewById(R.id.notification_location);
            countNotificationTextView = itemView.findViewById(R.id.notification_count_textView);
            this.listener = listener;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClicked(getAdapterPosition());
                }
            });
        }
    }

    public interface onNotificationClicked{
        public void onClicked(int position);
    }
}
