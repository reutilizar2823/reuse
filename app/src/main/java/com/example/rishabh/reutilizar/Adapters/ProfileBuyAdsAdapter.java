package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rishabh.reutilizar.Models.ProfileAdsDetails;
import com.example.rishabh.reutilizar.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/8/17.
 */

public class ProfileBuyAdsAdapter extends RecyclerView.Adapter<ProfileBuyAdsAdapter.ProfileAdsViewHolder>  {
    Context context;
    AdClicked mListener;
    ArrayList<ProfileAdsDetails> arrayList;

    public ProfileBuyAdsAdapter(Context context, AdClicked mListener, ArrayList<ProfileAdsDetails> arrayList){
        this.context=context;
        this.mListener=mListener;
        this.arrayList=arrayList;
    }

    @Override
    public ProfileAdsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.profile_buy_ad_view,parent,false);
        return new ProfileAdsViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(ProfileAdsViewHolder holder, int position) {
            holder.schoolNameTextView.setText(arrayList.get(position).getSchoolName());
            holder.classNameTextView.setText(arrayList.get(position).getClassName()+" Class");
            holder.locationTextView.setText(arrayList.get(position).getLocation());
            String dateTime = arrayList.get(position).getDateTime();
            char lastChar = dateTime.charAt(dateTime.length()-1);
            char secLastChar = dateTime.charAt(dateTime.length()-2);
            if(lastChar=='M' && (secLastChar=='A' || secLastChar=='P')){
                String[] stringArr = dateTime.split(" ");
                String str = "";
                //Leave Posted on
                for(int i=2;i<stringArr.length;i++){
                     str += stringArr[i]+" ";
                }
                holder.dateTimeTextView.setText(str);
            }
            else {
                holder.dateTimeTextView.setText(dateTime);
            }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ProfileAdsViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        TextView schoolNameTextView;
        TextView classNameTextView;
        TextView locationTextView;
        TextView dateTimeTextView;
        ImageView deleteImageView;
        AdClicked mListener;

         ProfileAdsViewHolder(View itemView, final AdClicked mListener) {
            super(itemView);
            schoolNameTextView=itemView.findViewById(R.id.buy_ad_school_name);
            classNameTextView=itemView.findViewById(R.id.buy_ad_class_name);
            locationTextView=itemView.findViewById(R.id.buy_ad_location);
            dateTimeTextView=itemView.findViewById(R.id.buy_ad_dateTime);
            deleteImageView=itemView.findViewById(R.id.profile_delete_buy_ad_imageButton);
            itemView.setOnClickListener(this);
            this.mListener=mListener;
             deleteImageView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     int pos = getAdapterPosition();
                     mListener.onDeleteButtonClicked(view,pos);
                 }
             });

        }


        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            mListener.onItemViewClicked(pos);
        }
    }

    public interface AdClicked  {
        void onDeleteButtonClicked(View view,int position);
        void onItemViewClicked(int position);
    }
}
