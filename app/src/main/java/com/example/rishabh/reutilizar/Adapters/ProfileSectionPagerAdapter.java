package com.example.rishabh.reutilizar.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.rishabh.reutilizar.Fragment.ProfileBuyAdsFragment;
import com.example.rishabh.reutilizar.Fragment.ProfileSellAdFragment;
import com.example.rishabh.reutilizar.Fragment.SellFragment;

/**
 * Created by rishabh on 10/4/17.
 */

public class ProfileSectionPagerAdapter extends FragmentPagerAdapter{

    public ProfileSectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==1){
            ProfileSellAdFragment fragment = new ProfileSellAdFragment();
            return fragment;
        }
        if(position==0){

            ProfileBuyAdsFragment fragment = new ProfileBuyAdsFragment();
            return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Buy Ads";
            case 1:
                return "Sell Ads";

        }
        return null;
    }
}
