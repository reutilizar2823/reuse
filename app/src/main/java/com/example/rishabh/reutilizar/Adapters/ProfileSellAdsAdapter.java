package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rishabh.reutilizar.Models.ProfileAdsDetails;
import com.example.rishabh.reutilizar.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 10/29/17.
 */

public class ProfileSellAdsAdapter extends RecyclerView.Adapter<ProfileSellAdsAdapter.ProfileSellAdsViewHolder> {
    Context context;
    SellAdClicked mListener;
    ArrayList<ProfileAdsDetails> arrayList;

    public ProfileSellAdsAdapter(Context context, SellAdClicked mListener, ArrayList<ProfileAdsDetails> arrayList){
        this.context=context;
        this.mListener=mListener;
        this.arrayList=arrayList;
    }


    @Override
    public ProfileSellAdsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.profile_sell_ad_view,parent,false);
        return new ProfileSellAdsViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(ProfileSellAdsViewHolder holder, int position) {
        holder.schoolNameTextView.setText(arrayList.get(position).getSchoolName());
        holder.classNameTextView.setText(arrayList.get(position).getClassName()+" Class");
        holder.locationTextView.setText(arrayList.get(position).getLocation());
        holder.priceBookSetTextView.setText("₹ "+arrayList.get(position).getPrice());
        String dateTime = arrayList.get(position).getDateTime();
        char lastChar = dateTime.charAt(dateTime.length()-1);
        char secLastChar = dateTime.charAt(dateTime.length()-2);
        if(lastChar=='M' && (secLastChar=='A' || secLastChar=='P')){
            String[] stringArr = dateTime.split(" ");
            String str = "";
            //Leave Posted on
            for(int i=2;i<stringArr.length;i++){
                str += stringArr[i]+" ";
            }
            holder.dateTimeTextView.setText(str);
        }
        else {
            holder.dateTimeTextView.setText(dateTime);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ProfileSellAdsViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        TextView schoolNameTextView;
        TextView classNameTextView;
        TextView locationTextView;
        TextView dateTimeTextView;
        TextView priceBookSetTextView;
        ImageView deleteImageView;
        SellAdClicked mListener;

        public ProfileSellAdsViewHolder(View itemView, final SellAdClicked mListener) {
            super(itemView);
            schoolNameTextView=itemView.findViewById(R.id.sell_ad_school_name);
            classNameTextView=itemView.findViewById(R.id.sell_ad_class_name);
            locationTextView=itemView.findViewById(R.id.sell_ad_location);
            dateTimeTextView=itemView.findViewById(R.id.sell_ad_dateTime);
            priceBookSetTextView = itemView.findViewById(R.id.sell_ad_priceBookSet);
            deleteImageView=itemView.findViewById(R.id.profile_delete_sell_ad_imageButton);
            this.mListener=mListener;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    mListener.adClicked(view,pos);
                }
            });
            deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    mListener.onDeleteButtonClicked(view,pos);
                }
            });
            itemView.setOnLongClickListener(this);

        }

        @Override
        public boolean onLongClick(View view) {
            int pos = getAdapterPosition();
            mListener.onMyLongClicked(pos);
            return false;
        }
    }

    public interface SellAdClicked  {
        void adClicked(View view,int position);
        void onDeleteButtonClicked(View view,int position);
        void onMyLongClicked(int position);
    }
}
