package com.example.rishabh.reutilizar.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.rishabh.reutilizar.Fragment.BuyFragment;
import com.example.rishabh.reutilizar.Fragment.SellFragment;

/**
 * Created by rishabh on 9/2/17.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter{


    public SectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            SellFragment fragment = new SellFragment();
            return fragment;
        }
        if(position==1){
            BuyFragment fragment = new BuyFragment();
            return fragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Sell Book";
            case 1:
            return "Buy Book";
        }
        return null;
    }
}
