package com.example.rishabh.reutilizar.Adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

/**
 * Created by rishabh on 10/1/17.
 */

public class SellAdAdapter extends RecyclerView.Adapter<SellAdAdapter.SellAdViewHolder> {
    ArrayList<OneBookDetail> booksDetailArrayList;
    Context context;
    String sellId;
    String schoolName;
    String className;
    NotifyInterface listener;
    BookViewClickListener mListener;

    public SellAdAdapter(Context context, ArrayList<OneBookDetail> booksDetailArrayList,String sellId,
                         String schoolName,String className , NotifyInterface listener,BookViewClickListener mListener){
        this.context=context;
        this.booksDetailArrayList=booksDetailArrayList;
        this.sellId = sellId;
        this.schoolName=schoolName;
        this.className=className;
        this.listener = listener;
        this.mListener = mListener;
    }

    @Override
    public SellAdViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.show_book_view,parent,false);
        return new SellAdViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final SellAdViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        holder.titleTextView.setText(booksDetailArrayList.get(position).title);
        holder.conditionTextView.setText(booksDetailArrayList.get(position).condition);
        if(booksDetailArrayList.get(position).condition.equals("Good")){
            holder.conditionTextView.setTextColor(ContextCompat.getColor(context, R.color.good_condition));
        }else if(booksDetailArrayList.get(position).condition.equals("Average")){
            holder.conditionTextView.setTextColor(ContextCompat.getColor(context, R.color.average_condition));
        }else if(booksDetailArrayList.get(position).condition.equals("Poor")){
            holder.conditionTextView.setTextColor(ContextCompat.getColor(context, R.color.poor_condition));
        }
        holder.descTextView.setText(booksDetailArrayList.get(position).description);
        if(booksDetailArrayList.get(position).description.equals("")){
            holder.descTextView.setText("No Description");
        }else{
            holder.descTextView.setText(booksDetailArrayList.get(position).description);
        }
        holder.progressBar.setVisibility(View.VISIBLE);
        if(!booksDetailArrayList.get(position).bookUrl.equals("")){
           //Image Url Present . Load Image Directly from Server
         Uri bookUri = Uri.parse(booksDetailArrayList.get(position).bookUrl);
       //  holder.progressBar.setVisibility(View.VISIBLE);
         Glide.with(context).load(bookUri).listener(new RequestListener<Drawable>() {
             @Override
             public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                 holder.progressBar.setVisibility(View.GONE);
                 return false;
             }

             @Override
             public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                 holder.progressBar.setVisibility(View.GONE);
                 return false;
             }
         })
           .into(holder.bookImageView);
       }

       else{
           File file = new File(booksDetailArrayList.get(position).bookPath);
           if(file.exists()  && booksDetailArrayList.get(position).isImageUpload==false){
               booksDetailArrayList.get(position).isImageUpload=true;
              // holder.progressBar.setVisibility(View.VISIBLE);
               try {
                   file = new Compressor(context).compressToFile(file);
               } catch (Exception e) {
                   e.printStackTrace();
               }
               Uri fileUri = Uri.fromFile(file);
               StorageReference reference = FirebaseStorage.getInstance().getReference();
               UploadTask uploadTask = reference.child("books/" + schoolName + "/" + className + "/" +
                       sellId + "/" + fileUri.getLastPathSegment()).putFile(fileUri);
               uploadTask.addOnFailureListener(new OnFailureListener() {
                   @Override
                   public void onFailure(@NonNull Exception e) {
                       holder.progressBar.setVisibility(View.GONE);
                       booksDetailArrayList.get(position).isImageUpload=false;
                   }
               }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                       Uri bookUri = taskSnapshot.getDownloadUrl();
                       String url = bookUri.toString();
                       DatabaseReference bookRefernce = Client.getmSchoolDatabaseReference().child(schoolName).child(className).child("sell")
                               .child(Client.getmFirebaseUser().getUid()).child(sellId).child(booksDetailArrayList.get(position).bookId)
                               .child("bookUrl");
                       bookRefernce.setValue(url);
                       SQLiteDatabase sqliteDatabase = DatabaseClient.getWritableDatabase(context);
                       ContentValues contentValues = new ContentValues();
                       contentValues.put(OpenHelper.IMAGE_URL, url);
                       String[] selectionArgs = {sellId, booksDetailArrayList.get(position).bookId};
                       sqliteDatabase.update(OpenHelper.BOOK_DETAILS_TABLE_NAME, contentValues, OpenHelper.SELL_ID
                               + " = ? " + " AND " + OpenHelper.BOOK_ID + " = ? ", selectionArgs);
                       holder.progressBar.setVisibility(View.GONE);
                       booksDetailArrayList.get(position).bookUrl = url;
                       listener.imageUploaded(position);
                   }
               });
           }else{
               //Set Default Image
               if(!file.exists()){
                   holder.progressBar.setVisibility(View.GONE);
                   holder.bookImageView.setImageResource(R.drawable.default_book);
               }

           }
       }
    }

    @Override
    public int getItemCount() {
        return booksDetailArrayList.size();
    }

    class SellAdViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView bookImageView;
        TextView titleTextView;
        TextView conditionTextView;
        TextView descTextView;
        ProgressBar progressBar;
        LinearLayout descLinearLayout;
        BookViewClickListener mListener;
        public SellAdViewHolder(View itemView,BookViewClickListener mListener) {
            super(itemView);
            this.mListener = mListener;
            bookImageView=itemView.findViewById(R.id.one_book_imageView);
            bookImageView.setOnClickListener(this);
            titleTextView=itemView.findViewById(R.id.one_book_title_textView);
            conditionTextView=itemView.findViewById(R.id.one_book_condition_textView);
            descTextView=itemView.findViewById(R.id.one_book_desc_textView);
            descLinearLayout=itemView.findViewById(R.id.description_linearLayout);
            descLinearLayout.setOnClickListener(this);
            progressBar=itemView.findViewById(R.id.book_image_progressBar);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            int id = view.getId();
            if(id==R.id.description_linearLayout){
                mListener.onDescriptionClicked(pos);
            }else if(id==R.id.one_book_imageView){
                mListener.onBookImageClicked(pos);
            }
        }
    }


    public interface NotifyInterface{
        public void imageUploaded(int position);
    }
    public interface BookViewClickListener{
        void onBookImageClicked(int position);
        void onDescriptionClicked(int position);
    }

}
