package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rishabh.reutilizar.Models.OneBookDetail;
import com.example.rishabh.reutilizar.Models.SellersBasicInfo;
import com.example.rishabh.reutilizar.R;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by rishabh on 9/9/17.
 */

public class SellerDetailsAdapter extends RecyclerView.Adapter<SellerDetailsAdapter.SellerDetailsViewHolder> {
    Context context;
    ArrayList<SellersBasicInfo> sellerDetailsArrayList;
    SellerViewClickListener mListener;
    long lastSeenTimeStamp;
    public SellerDetailsAdapter(Context context,ArrayList<SellersBasicInfo> sellerDetailsArrayList,SellerViewClickListener mListener){
        this.context=context;
        this.sellerDetailsArrayList=sellerDetailsArrayList;
        this.mListener=mListener;
        lastSeenTimeStamp=Long.MAX_VALUE;
    }

    public SellerDetailsAdapter(Context context,ArrayList<SellersBasicInfo> sellerDetailsArrayList,SellerViewClickListener mListener
        ,long lastSeenTimeStamp){
        this.context=context;
        this.sellerDetailsArrayList=sellerDetailsArrayList;
        this.mListener=mListener;
        this.lastSeenTimeStamp=lastSeenTimeStamp;
    }


    @Override
    public SellerDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.seller_view,parent,false);
        return new SellerDetailsViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(SellerDetailsViewHolder holder, int position) {
        holder.nameTextView.setText(sellerDetailsArrayList.get(position).name);
        /*if(sellerDetailsArrayList.get(position).status.equals("partial"))
           holder.statusTextView.setText("Partial Book Set");
        else{
            holder.statusTextView.setText("Complete Book Set");
        }*/
        holder.adDateTextView.setText(sellerDetailsArrayList.get(position).actualTime);
        if(sellerDetailsArrayList.get(position).price.equals("0")){
            holder.priceTextView.setText("FREE");
        }else{
            holder.priceTextView.setText("₹ " + sellerDetailsArrayList.get(position).price);
        }

        double distance = sellerDetailsArrayList.get(position).distance;
        DecimalFormat df = new DecimalFormat("0.0");
        holder.distanceTextView.setText(df.format(distance)+" KM");
        if(Long.parseLong(sellerDetailsArrayList.get(position).timeStamp)>= lastSeenTimeStamp){
            holder.sellerViewLinearLayout.setBackgroundResource(R.color.notificationBackground);
        }else{
            holder.sellerViewLinearLayout.setBackgroundResource(R.color.white);
        }
    }



    @Override
    public int getItemCount() {
        return sellerDetailsArrayList.size();
    }

    public static class SellerDetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView distanceTextView;
        TextView nameTextView;
        TextView priceTextView;
       // TextView statusTextView;
        TextView moreDetailsTextView;
        TextView adDateTextView;
        LinearLayout sellerViewLinearLayout;
        SellerViewClickListener mListener;


        public SellerDetailsViewHolder(View itemView,SellerViewClickListener mListener) {
            super(itemView);
            this.mListener=mListener;
            distanceTextView=itemView.findViewById(R.id.seller_view_distance);
            nameTextView=itemView.findViewById(R.id.seller_view_name);
            priceTextView=itemView.findViewById(R.id.seller_view_price);
           // statusTextView=itemView.findViewById(R.id.seller_view_status);
            moreDetailsTextView=itemView.findViewById(R.id.seller_view_more_details_textView);
            adDateTextView = itemView.findViewById(R.id.seller_view_ad_date);
            sellerViewLinearLayout=itemView.findViewById(R.id.seller_view_main_layout);
            moreDetailsTextView.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mListener.onButtonClicked(position);
        }
    }

   public  interface SellerViewClickListener{
        void onButtonClicked(int position);
    }
}
