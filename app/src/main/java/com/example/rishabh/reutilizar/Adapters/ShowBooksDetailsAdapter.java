package com.example.rishabh.reutilizar.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.rishabh.reutilizar.Activities.SellerBookDetailsActivity;
import com.example.rishabh.reutilizar.Models.ShowBookDetail;
import com.example.rishabh.reutilizar.R;

import java.util.ArrayList;

/**
 * Created by rishabh on 9/10/17.
 */

public class ShowBooksDetailsAdapter extends RecyclerView.Adapter<ShowBooksDetailsAdapter.OneBookViewHolder>{

    Context context;
    ArrayList<ShowBookDetail> showBookDetailArrayList;
    BookViewClickListener mListener;

    public ShowBooksDetailsAdapter(Context context, ArrayList<ShowBookDetail> showBookDetailArrayList
            , BookViewClickListener mListener){
        this.context=context;
        this.showBookDetailArrayList=showBookDetailArrayList;
        this.mListener=mListener;
    }

    @Override
    public OneBookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.show_book_view,parent,false);
        return new OneBookViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final OneBookViewHolder holder, final int position) {
        holder.bookTitleTextView.setText(showBookDetailArrayList.get(position).title);
        holder.bookConditionTextView.setText(showBookDetailArrayList.get(position).condition);
        if(showBookDetailArrayList.get(position).condition.equals("Good")){
            holder.bookConditionTextView.setTextColor(ContextCompat.getColor(context, R.color.good_condition));
        }else if(showBookDetailArrayList.get(position).condition.equals("Average")){
            holder.bookConditionTextView.setTextColor(ContextCompat.getColor(context, R.color.average_condition));
        }else if(showBookDetailArrayList.get(position).condition.equals("Poor")){
            holder.bookConditionTextView.setTextColor(ContextCompat.getColor(context, R.color.poor_condition));
        }
        if(showBookDetailArrayList.get(position).description.equals("")){
            holder.bookDescriptionTextView.setText("No Description");
        }else{
            holder.bookDescriptionTextView.setText(showBookDetailArrayList.get(position).description);
        }


        if(!showBookDetailArrayList.get(position).bookUrl.equals("")){
            Uri bookUri = Uri.parse(showBookDetailArrayList.get(position).bookUrl);
         holder.progressBar.setVisibility(View.VISIBLE);
         Glide.with(context).load(bookUri).listener(new RequestListener<Drawable>() {
             @Override
             public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                 //Set Reload button
                  holder.progressBar.setVisibility(View.GONE);
                 return false;
             }

             @Override
             public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                 holder.progressBar.setVisibility(View.GONE);
                 return false;
             }
         }).into(holder.bookImageView);
        }else{
            holder.bookImageView.setImageResource(R.drawable.default_book);
        }
    }

    @Override
    public int getItemCount() {
        return showBookDetailArrayList.size();
    }

    public class OneBookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView bookTitleTextView;
        TextView bookConditionTextView;
        TextView bookDescriptionTextView;
        ImageView bookImageView;
        ProgressBar progressBar;
        BookViewClickListener mListener;
        LinearLayout descLinearLayout;

        public OneBookViewHolder(View itemView,BookViewClickListener mListener) {
            super(itemView);
            this.mListener=mListener;
            bookTitleTextView=itemView.findViewById(R.id.one_book_title_textView);
            bookConditionTextView=itemView.findViewById(R.id.one_book_condition_textView);
            bookDescriptionTextView=itemView.findViewById(R.id.one_book_desc_textView);
            bookImageView=itemView.findViewById(R.id.one_book_imageView);
            bookImageView.setOnClickListener(this);
            progressBar = itemView.findViewById(R.id.book_image_progressBar);
            descLinearLayout = itemView.findViewById(R.id.description_linearLayout);
            descLinearLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            int id = view.getId();
            if(id==R.id.description_linearLayout){
                mListener.onDescriptionClicked(pos);
            }else if(id==R.id.one_book_imageView){
                mListener.onBookImageClicked(pos);
            }

        }
    }

    public interface BookViewClickListener{
        void onBookImageClicked(int position);
        void onDescriptionClicked(int position);
    }
}
