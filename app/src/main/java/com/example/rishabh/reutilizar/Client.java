package com.example.rishabh.reutilizar;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rishabh on 9/2/17.
 */

public class Client {

    private static SharedPreferences sharedPreferences ;
    private static FirebaseDatabase mFirebaseDatabase;
    private static DatabaseReference mUserDatabaseReference;
    private static FirebaseAuth mFirebaseAuth;
    private static FirebaseUser mFirebaseUser;
    private static DatabaseReference mSchoolDatabaseReference;
    private static StorageReference mStorageReference;

    public static StorageReference getmStorageReference() {
        mStorageReference = FirebaseStorage.getInstance().getReference();
        return mStorageReference;
    }

    public static  SharedPreferences getSharedPreferences(Context context) {
        if(sharedPreferences==null){
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME , Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public static FirebaseDatabase getmFirebaseDatabase() {
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        return mFirebaseDatabase;
    }

    public static DatabaseReference getmUserDatabaseReference() {
        mUserDatabaseReference = getmFirebaseDatabase().getReference(Constants.getUserDatabaseReference());
        return mUserDatabaseReference;
    }

    public static FirebaseAuth getmFirebaseAuth() {
        mFirebaseAuth = FirebaseAuth.getInstance();
        return mFirebaseAuth;
    }

    public static FirebaseUser getmFirebaseUser() {
        mFirebaseUser = getmFirebaseAuth().getCurrentUser();
        return mFirebaseUser;
    }
    public static void signOut(){

        mFirebaseUser=null;
        mFirebaseAuth=null;
        mFirebaseDatabase=null;
        mSchoolDatabaseReference=null;
        mStorageReference=null;

    }
    public static DatabaseReference getmSchoolDatabaseReference() {
            mSchoolDatabaseReference=getmFirebaseDatabase().getReference(Constants.getSchoolDatabaseReference()).
                    child(Constants.getCOUNTRY()).child(Constants.getCITY());
        return mSchoolDatabaseReference;
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private static int todayOrYesterday(long time){
        long millisecInADay = 1000*60*60*24;
       // Log.d("hola" , time+"");
        time += 19800000;
        int nofDays1 = (int)(time/millisecInADay);
        long currentTime = System.currentTimeMillis();
       // Log.d("hola" , "noOfDays1" + nofDays1);
       // Log.d("hola" , currentTime+"");
        currentTime += 19800000;
        int noOfDays2 = (int)(currentTime/millisecInADay);
        //Log.d("hola" , "noOfDays2" + noOfDays2);
        int diff =  (noOfDays2 - nofDays1);
        return diff;

    }
    public static String dateTime(long timeStamp) {
        int todayOrYesterday = todayOrYesterday(timeStamp);
        if(todayOrYesterday==0){
            return  "Posted Today ";
        }
        else if(todayOrYesterday==1){
            return "Posted Yesterday ";
        }
        else if(todayOrYesterday==2){
            return "Posted 2 days ago ";
        }
        else if(todayOrYesterday==3){
            return "Posted 3 days ago ";
        }
        else if(todayOrYesterday==4){
            return "Posted 4 days ago ";
        }
        else if(todayOrYesterday==5){
            return "Posted 5 days ago ";
        }
        else if(todayOrYesterday==6){
            return "Posted 6 days ago ";
        }
        else if(todayOrYesterday==7){
            return "Posted a week ago ";
        }
        Date date = new Date(timeStamp);
        Format format = new SimpleDateFormat("dd-MM-yyyy");
        String day=format.format(date);
        String[] dayArr = day.split("-");
        String finalDate=dayArr[0]+"-";
        switch(dayArr[1]){
            case "01":
                finalDate+="Jan-";
                break;
            case "02":
                finalDate+="Feb-";
                break;
            case "03":
                finalDate+="Mar-";
                break;
            case "04":
                finalDate+="Apr-";
                break;
            case "05":
                finalDate+="May-";
                break;
            case "06":
                finalDate+="June-";
                break;
            case "07":
                finalDate+="July-";
                break;
            case "08":
                finalDate+="Aug-";
                break;
            case "09":
                finalDate+="Sep-";
                break;
            case "10":
                finalDate+="Oct-";
                break;
            case "11":
                finalDate+="Nov-";
                break;
            case "12":
                finalDate+="Dec-";
                break;
            default:
                finalDate+="Apr-";
        }
        finalDate+=dayArr[2]+", ";
        Format format2 = new SimpleDateFormat("HH:mm");
        String time = format2.format(date);
        String timeArr[] = time.split(":");
        int hour24 = Integer.parseInt(timeArr[0]);
        int min = Integer.parseInt(timeArr[1]);
        int hour12=hour24%12;
        if(hour24==12){
            finalDate+="12";
        }
        else if(hour12<10){
            finalDate+="0"+String.valueOf(hour12);
        }else{
            finalDate+=String.valueOf(hour12);
        }
        finalDate+=":";
        if(min<10){
            finalDate+="0"+String.valueOf(min);
        }
        else{
            finalDate+=String.valueOf(min);
        }
        if(hour24/12==1){
            finalDate+=" PM";
        }else{
            finalDate+=" AM";
        }
        return "Posted on " + finalDate;
    }
}
