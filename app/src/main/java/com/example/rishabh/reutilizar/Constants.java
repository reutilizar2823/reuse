package com.example.rishabh.reutilizar;

/**
 * Created by rishabh on 8/28/17.
 */

public class Constants {
    public static final int SIGN_IN_ACTIVITY = 1;
    public static final  String SHARED_PREFERENCE_NAME = "name_file";
    public static final  String NAME = "name";
    private static final String USER_DATABASE_REFERENCE = "users";
    private static final String SCHOOL_DATABASE_REFERENCE = "schools";
    private static final  String COUNTRY = "India";
    private static final String CITY = "New Delhi";
    public static final String SELL_DETAILS = "sell_details";
    public static final String BUY_DETAILS = "buy_details";
    public static final String ONE_BOOK_DETAILS="one_book_details";
    public static  final String SELLER_DETAILS = "seller_details";
    public static final String SHOW_BOOKS ="show_books";
    private static final String BEST_BOOK_CONDITION = "Good";
    private static final String MEDIUM_BOOK_CONDITION = "Average";
    private static final String WORST_BOOK_CONDITION= "Poor";
    public static final int NOTIFICATION_ACTIVITY=101;
    public static final int GENERAL_ACTIVITY=100;
    public static  final String LAUNCHING_ACTIVITY = "launchingActivity";
    public static final int PARTIAL_BOOK_ACTIVITY=120;


    public static String getUserDatabaseReference() {
        return USER_DATABASE_REFERENCE;
    }

    public static String getSchoolDatabaseReference(){
        return SCHOOL_DATABASE_REFERENCE;
    }

    public static String getCOUNTRY() {
        return COUNTRY;
    }

    public static String getCITY() {
        return CITY;
    }

    public static String getBestBookCondition() {
        return BEST_BOOK_CONDITION;
    }

    public static String getMediumBookCondition() {
        return MEDIUM_BOOK_CONDITION;
    }

    public static String getWorstBookCondition() {
        return WORST_BOOK_CONDITION;
    }
}
