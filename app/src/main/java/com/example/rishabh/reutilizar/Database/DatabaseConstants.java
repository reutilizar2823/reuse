package com.example.rishabh.reutilizar.Database;

/**
 * Created by rishabh on 9/7/17.
 */

public class DatabaseConstants {
    public static final String PARTIAL_BOOK_STATUS = "partial";
    public static final String COMPLETE_BOOK_STATUS = "complete";
    public static final String SELL_CATEGORY = "sell";
    public static final String BUY_CATEGORY = "buy";
}
