package com.example.rishabh.reutilizar.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by rishabh on 9/2/17.
 */

public class OpenHelper extends SQLiteOpenHelper {
    //USER INFO TABLE ATTRIBUTES
   /* public static final String USER_TABLE_NAME = "user";
    public static final String USER_NAME = "name";
    public static final String USER_NUMBER = "number";
    public static final String USER_UID = "uid";
    public static final String USER_PHOTO_URL = "photo_url";*/

    //USER SELL/BUY DETAILS TABLE ATTRIBUTES
    public static final String USER_SELLBUY_TABLE_NAME="sell_buy";
    public static final String SCHOOL ="school";
    public static final String CLASS="class";
    public static final String LOCATION="location";
    public static final String SELLBUY_ID="sellBuy_id";
    public static final String CATEGORY="category";
    public static final String STATUS="status";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static  final String TIME_STAMP="timeStamp";
    public static  final String LAST_SEEN_TIME_STAMP = "lastSeenTimeStamp";
    public static final String PRICE_BOOK_SET = "price_book_set";
    //USER SELL BOOK DETAILS TABLE ATTRIBUTES
    public static final String BOOK_DETAILS_TABLE_NAME="bookDetails";
    public static final String SELL_ID="sellId";
    public static final String BOOK_ID = "bookId";
    public static final String TITLE="title";
    public static final String CONDITION="condition";
    public static final String DESCRIPTION="description";
    public static final String IMAGE_URL="imageUrl";
    public static final String IMAGE_PATH="imagePath";



    public OpenHelper(Context context) {
        super(context,"Reutilizar.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //USER INFO TABLE
        /*String table1 = "CREATE TABLE "+USER_TABLE_NAME+" ( "+USER_NAME+" text, "+USER_NUMBER+" text, "+USER_UID+
                " text, "+USER_PHOTO_URL+" text);";
        Log.d("check",table1);
        db.execSQL(table1);*/

        //USER SELL/BUY DETAILS TABLE
        String table2 = "CREATE TABLE "+USER_SELLBUY_TABLE_NAME+" ( "+SCHOOL+" text, "+CLASS+" text, "+LOCATION
                +" text, "+LATITUDE+" real, "+LONGITUDE+" real, "+SELLBUY_ID+" text, "+CATEGORY+" text, "+STATUS+" text, "
                +TIME_STAMP+" text, " + LAST_SEEN_TIME_STAMP+" text, " + PRICE_BOOK_SET + " text);" ;

        db.execSQL(table2);

        //USER SELL BOOK DETAILS TABLE
        String table3 = "CREATE TABLE "+BOOK_DETAILS_TABLE_NAME+" ( "+SELL_ID+" text, "+TITLE+" text, "
                +CONDITION+" text, "+DESCRIPTION+" text, "+IMAGE_URL+" text, "+IMAGE_PATH+" text, "
                +BOOK_ID+" text);";

        db.execSQL(table3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
