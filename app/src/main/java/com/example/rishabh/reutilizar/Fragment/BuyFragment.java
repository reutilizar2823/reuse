package com.example.rishabh.reutilizar.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rishabh.reutilizar.Activities.BuyFormActivity;
import com.example.rishabh.reutilizar.Activities.SellFormActivity;
import com.example.rishabh.reutilizar.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuyFragment extends Fragment implements View.OnClickListener {

    private Button buyBookButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buy, container, false);
        buyBookButton=(Button)view.findViewById(R.id.buy_book_button);
        buyBookButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.buy_book_button){
            startActivity(new Intent(getActivity(), BuyFormActivity.class));
        }
    }
}
