package com.example.rishabh.reutilizar.Fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.rishabh.reutilizar.Adapters.BuyerDetailsAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Constants;
import com.example.rishabh.reutilizar.Models.BuyerBasicInfo;
import com.example.rishabh.reutilizar.Models.BuyerDistanceSort;
import com.example.rishabh.reutilizar.Models.BuyerTimeSort;
import com.example.rishabh.reutilizar.Models.SellerDistanceSort;
import com.example.rishabh.reutilizar.Models.SellerTimeSort;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class PotentialBuyerFragment extends Fragment implements BuyerDetailsAdapter.BuyerDetailsClickListener {
    private RecyclerView mRecyclerView;
    private ArrayList<BuyerBasicInfo> buyerBasicInfoArrayList;
    private UserRequestDetails userRequestDetails;
    private BuyerDetailsAdapter mAdapter;
    ProgressBar mProgressBar;
    LinearLayout noBuyerLinearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view =  inflater.inflate(R.layout.fragment_potential_buyer, container, false);
        Bundle bundle = getArguments();
        userRequestDetails= (UserRequestDetails) bundle.getSerializable(Constants.SELLER_DETAILS);
        mRecyclerView=view.findViewById(R.id.potential_buyer_recyclerView);
        buyerBasicInfoArrayList = new ArrayList<>();
        mProgressBar=view.findViewById(R.id.buyer_progressBar);
        noBuyerLinearLayout=view.findViewById(R.id.no_buyer_linearLayout);
        showProgress(true);
        mAdapter=new BuyerDetailsAdapter(getContext(),buyerBasicInfoArrayList,this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        DatabaseReference mDatabaseReference = Client.getmSchoolDatabaseReference()
                .child(userRequestDetails.getSchoolName()).
                        child(userRequestDetails.getClassName()).child("buy");
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChildren()){
                    showProgress(false);
                    noBuyerLinearLayout.setVisibility(View.VISIBLE);
                    return;
                }
                Iterable<DataSnapshot> userDataSnapshot = dataSnapshot.getChildren();
                for(DataSnapshot oneUserDataSnapshot : userDataSnapshot){
                    Object nameObj = oneUserDataSnapshot.child("name").getValue();
                    if(nameObj==null){
                        continue;
                    }
                    String name = nameObj.toString();
                    Object numberObj = oneUserDataSnapshot.child("number").getValue();
                    if(numberObj==null){
                        continue;
                    }
                    String number = numberObj.toString();
                    Iterable<DataSnapshot> buyDataSnapshots = oneUserDataSnapshot.getChildren();
                    for(DataSnapshot onebuyDatasnapshot : buyDataSnapshots){
                        if(!onebuyDatasnapshot.getKey().equals("name") &&
                                !onebuyDatasnapshot.getKey().equals("number")&&
                                !onebuyDatasnapshot.getKey().equals("photoUrl")) {
                            Object locationObj =onebuyDatasnapshot.child("location").getValue();
                            if(locationObj==null){
                                continue;
                            }
                            String location = locationObj.toString();
                            Object latObj = onebuyDatasnapshot.child("lat").getValue();
                            if(latObj==null){
                                continue;
                            }
                            String latString =  latObj.toString();
                            Object lngObj = onebuyDatasnapshot.child("lng").getValue();
                            if(lngObj==null){
                                continue;
                            }
                            String lngString =  lngObj.toString();
                            double lat = Double.parseDouble(latString);
                            double lng =Double.parseDouble(lngString);
                            Object timeStampObj =onebuyDatasnapshot.child("timeStamp").getValue();
                            if(timeStampObj==null){
                                continue;
                            }
                            String timeStamp = timeStampObj.toString();
                            long timeStampp = Long.parseLong(timeStamp);
                            String actualTime = Client.dateTime(timeStampp);
                            BuyerBasicInfo buyerBasicInfo = new BuyerBasicInfo(name, number, lat, lng, location
                            ,timeStamp,actualTime);
                            buyerBasicInfoArrayList.add(buyerBasicInfo);
                        }
                    }
                }

                for (BuyerBasicInfo buyerBasicInfo : buyerBasicInfoArrayList){
                    double lat = Double.parseDouble(userRequestDetails.getLat());
                    double lng = Double.parseDouble(userRequestDetails.getLng());
                    buyerBasicInfo.distance = distance(lat,lng, buyerBasicInfo.lat,buyerBasicInfo.lng);
                }

                Collections.sort(buyerBasicInfoArrayList , new BuyerDistanceSort());
                mAdapter.notifyDataSetChanged();
                showProgress(false);
                if(buyerBasicInfoArrayList.size()==0){
                    noBuyerLinearLayout.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       inflater.inflate(R.menu.filter_menu,menu);
        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.sort_distance){
            Collections.sort(buyerBasicInfoArrayList , new BuyerDistanceSort());
            mAdapter.notifyDataSetChanged();

        }else if(item.getItemId() == R.id.sort_date){
            Collections.sort(buyerBasicInfoArrayList , new BuyerTimeSort());
            mAdapter.notifyDataSetChanged();
        }
        return true;
    }


    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return Math.abs(dist);
    }


    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }


    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    private void showProgress(boolean show){
        mProgressBar.setVisibility(show? View.VISIBLE: View.GONE);
    }

    @Override
    public void onItemViewClicked(int position) {
        new LovelyInfoDialog(getContext())
                .setIcon(R.drawable.description_icon)
                .setTopColorRes(R.color.colorPrimary)
                .setTitle("Buyer Details")
                .setMessage("Name : " + buyerBasicInfoArrayList.get(position).name + "\n\n"
                        + "Address : "  + buyerBasicInfoArrayList.get(position).location )
                .show();
    }

    @Override
    public void onPhoneClicked(int position) {
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
        phoneIntent.setData(Uri.parse("tel:"+buyerBasicInfoArrayList.get(position).number));
        startActivity(phoneIntent);
    }

}
