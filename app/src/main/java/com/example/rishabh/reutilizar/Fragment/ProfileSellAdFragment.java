package com.example.rishabh.reutilizar.Fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.rishabh.reutilizar.Activities.PreviewSellAd;
import com.example.rishabh.reutilizar.Adapters.ProfileBuyAdsAdapter;
import com.example.rishabh.reutilizar.Adapters.ProfileSellAdsAdapter;
import com.example.rishabh.reutilizar.Client;
import com.example.rishabh.reutilizar.Database.DatabaseClient;
import com.example.rishabh.reutilizar.Database.DatabaseConstants;
import com.example.rishabh.reutilizar.Database.OpenHelper;
import com.example.rishabh.reutilizar.Models.ProfileAdsDetails;
import com.example.rishabh.reutilizar.Models.UserRequestDetails;
import com.example.rishabh.reutilizar.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProfileSellAdFragment extends Fragment implements  ProfileSellAdsAdapter.SellAdClicked {
    private RecyclerView mRecyclerView;
    private ProfileSellAdsAdapter adapter;
    private ArrayList<ProfileAdsDetails> arrayList;
    private View frameview;
    //private RelativeLayout frameview;
    private LinearLayout noSellAdLinearLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_sell_ad, container, false);
        mRecyclerView=view.findViewById(R.id.profile_sellAd_RecyclerView);
        frameview = view.findViewById(R.id.profile_sell_framelayout);
        noSellAdLinearLayout=view.findViewById(R.id.profile_no_sell_ad_linearLayout);
        arrayList = fetchDataFromDatabase();
        if(arrayList.size()==0){
            noSellAdLinearLayout.setVisibility(View.VISIBLE);
            return view;
        }
        adapter = new ProfileSellAdsAdapter(getActivity(),this,arrayList);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        return view;
    }

    private ArrayList<ProfileAdsDetails> fetchDataFromDatabase() {
        ArrayList<ProfileAdsDetails> arrayList = new ArrayList<>();
        SQLiteDatabase database = DatabaseClient.getWritableDatabase(getActivity());
        String selectionArgs[]={DatabaseConstants.SELL_CATEGORY};
        Cursor cursor = database.query(OpenHelper.USER_SELLBUY_TABLE_NAME,null,OpenHelper.CATEGORY+" = ?",
                selectionArgs,null,null,null);
        while(cursor.moveToNext()){
            String schoolName = cursor.getString(cursor.getColumnIndex(OpenHelper.SCHOOL));
            String className = cursor.getString(cursor.getColumnIndex(OpenHelper.CLASS));
            String location = cursor.getString(cursor.getColumnIndex(OpenHelper.LOCATION));
            String lat = cursor.getString(cursor.getColumnIndex(OpenHelper.LATITUDE));
            String lng = cursor.getString(cursor.getColumnIndex(OpenHelper.LONGITUDE));
            String status = cursor.getString(cursor.getColumnIndex(OpenHelper.STATUS));
            String sell_buyId = cursor.getString(cursor.getColumnIndex(OpenHelper.SELLBUY_ID));
            String timeStamp = cursor.getString(cursor.getColumnIndex(OpenHelper.TIME_STAMP));
            long timeStampLong = Long.parseLong(timeStamp);
            String stringTime = Client.dateTime(timeStampLong);
            String price = cursor.getString(cursor.getColumnIndex(OpenHelper.PRICE_BOOK_SET));
            ProfileAdsDetails profileAdsDetails = new ProfileAdsDetails(schoolName,className,location,lat,
                    lng,status,sell_buyId , price , stringTime);
            arrayList.add(profileAdsDetails);
        }
        cursor.close();
        return arrayList;
    }




    @Override
    public void adClicked(View view, int position) {
        Intent intent = new Intent(getActivity(), PreviewSellAd.class);
        intent.putExtra("sellId",arrayList.get(position).getSell_buyId());
        UserRequestDetails userRequestDetails = new UserRequestDetails(arrayList.get(position).getSchoolName() ,
                arrayList.get(position).getClassName() , arrayList.get(position).getLocation(),
                arrayList.get(position).getLat() , arrayList.get(position).getLng());
        intent.putExtra("userDetails" , userRequestDetails);
        startActivity(intent);
    }

    @Override
    public void onDeleteButtonClicked(final View view, final int position) {
        view.setEnabled(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete");
        builder.setMessage("Are you sure you want to delete this Sell Ad ?");
        builder.setCancelable(false);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!Client.haveNetworkConnection(getActivity())){
                    Snackbar.make(frameview , "No Internet Connection" , Snackbar.LENGTH_SHORT).show();
                    view.setEnabled(true);
                    return;
                }
                final ProgressDialog progress = new ProgressDialog(getActivity());
                progress.setMessage("Deleting.... ");
                progress.setCancelable(false);
                progress.show();
                String schoolName = arrayList.get(position).getSchoolName();
                String className = arrayList.get(position).getClassName();
                int count=0;
                for(ProfileAdsDetails userSellAds : arrayList){
                    if(userSellAds.getSchoolName().equals(schoolName) && userSellAds.getClassName().equals(className)){
                        count++;
                    }
                }
                Map<String ,Object> childUpdates = new HashMap<>();
                if(count==1){
                    childUpdates.put("/schools/India/New Delhi/" + arrayList.get(position).getSchoolName()
                            + "/" + arrayList.get(position).getClassName() + "/sell/" + Client.getmFirebaseUser().getUid(),
                            null);
                }else if(count>1){
                    childUpdates.put("/schools/India/New Delhi/" + arrayList.get(position).getSchoolName()
                            + "/" + arrayList.get(position).getClassName() + "/sell/" + Client.getmFirebaseUser().getUid()
                            +"/" + arrayList.get(position).getSell_buyId(), null);
                }
                childUpdates.put("/users/" + Client.getmFirebaseUser().getUid()
                        + "/sell/" + arrayList.get(position).getSell_buyId() , null);
                FirebaseDatabase.getInstance().getReference().updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if(databaseError==null){
                            SQLiteDatabase database = DatabaseClient.getWritableDatabase(getActivity());
                            String selectionArgs[] = { arrayList.get(position).getSell_buyId()};
                            database.delete(OpenHelper.USER_SELLBUY_TABLE_NAME , OpenHelper.SELLBUY_ID + " = ? " , selectionArgs);
                            database.delete(OpenHelper.BOOK_DETAILS_TABLE_NAME , OpenHelper.SELL_ID + " = ? " , selectionArgs);
                            arrayList.remove(position);
                            adapter.notifyItemRemoved(position);
                            if(progress!=null && progress.isShowing())
                                progress.dismiss();
                            if(arrayList.size()==0){
                                noSellAdLinearLayout.setVisibility(View.VISIBLE);
                            }
                        }else{
                            Toast.makeText(getActivity(), "Some Error Occured", Toast.LENGTH_SHORT).show();
                            if(progress!=null && progress.isShowing())
                                progress.dismiss();
                            view.setEnabled(true);
                        }
                    }
                });

            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                view.setEnabled(true);
            }
        });
        builder.create().show();
    }

    @Override
    public void onMyLongClicked(int position) {
        new LovelyInfoDialog(getContext())
                .setIcon(R.drawable.description_icon)
                .setTopColorRes(R.color.colorPrimary)
                .setTitle("Ad Details")
                .setMessage("School : " + arrayList.get(position).getSchoolName() + "\n\n"
                        + "Address : "  + arrayList.get(position).getLocation() +"\n\n"
                        + "Class : " + arrayList.get(position).getClassName() + "\n\n"
                        + "Price : " + "₹ "+arrayList.get(position).getPrice())
                .show();
    }


}
