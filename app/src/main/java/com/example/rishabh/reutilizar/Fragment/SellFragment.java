package com.example.rishabh.reutilizar.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rishabh.reutilizar.Activities.BuyFormActivity;
import com.example.rishabh.reutilizar.R;
import com.example.rishabh.reutilizar.Activities.SellFormActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellFragment extends Fragment implements View.OnClickListener {


    private Button sellBookButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_sell, container, false);
        sellBookButton = view.findViewById(R.id.sell_book_button);
        sellBookButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.sell_book_button){
            startActivity(new Intent(getActivity(), SellFormActivity.class));
        }
    }
}
