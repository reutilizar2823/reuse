package com.example.rishabh.reutilizar.Models;

/**
 * Created by rishabh on 9/10/17.
 */

public class BuyerBasicInfo {
    public String name;
    public String number;
    public double lat;
    public double lng;
    public String location;
    public double distance;
    public String timeStamp;
    public String actualTime;

    public BuyerBasicInfo(String name, String number, double lat, double lng, String location,
                          String timeStamp,String actualTime) {
        this.name = name;
        this.number = number;
        this.lat = lat;
        this.lng = lng;
        this.location = location;
        this.timeStamp = timeStamp;
        this.actualTime = actualTime;
    }
}
