package com.example.rishabh.reutilizar.Models;

/**
 * Created by rishabh on 9/10/17.
 */

public class BuyerDistanceSort implements java.util.Comparator<com.example.rishabh.reutilizar.Models.BuyerBasicInfo> {

    @Override
    public int compare(BuyerBasicInfo buyerBasicInfo1, BuyerBasicInfo buyerBasicInfo2) {
        if(buyerBasicInfo1.distance<buyerBasicInfo2.distance) return -1;
        else if(buyerBasicInfo1.distance>buyerBasicInfo2.distance) return 1;
        return 0;

    }


}