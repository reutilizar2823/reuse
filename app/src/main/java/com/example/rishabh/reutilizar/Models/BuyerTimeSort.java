package com.example.rishabh.reutilizar.Models;

import java.util.Comparator;

/**
 * Created by rishabh on 10/20/17.
 */

public class BuyerTimeSort implements Comparator<BuyerBasicInfo> {
    @Override
    public int compare(BuyerBasicInfo buyerBasicInfo1, BuyerBasicInfo buyerBasicInfo2) {
        long timeStamp1 = Long.parseLong(buyerBasicInfo1.timeStamp);
        long timeStamp2 = Long.parseLong(buyerBasicInfo2.timeStamp);
        if(timeStamp1 < timeStamp2 ) return  1;
        else if(timeStamp1 > timeStamp2) return -1;
        return 0;

    }
}
