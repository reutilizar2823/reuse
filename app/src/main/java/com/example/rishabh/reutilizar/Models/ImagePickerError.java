package com.example.rishabh.reutilizar.Models;

/**
 * Created by Daksh Garg on 12/25/2017.
 */

public enum ImagePickerError {
    NO_INTENTS_AVAILABLE, FAIL_TO_GENERATE_BITMAP
}
