package com.example.rishabh.reutilizar.Models;

import java.io.Serializable;

/**
 * Created by rishabh on 10/18/17.
 */

public class NotificationAdDetails implements Serializable{
    private String schoolName;
    private String className;
    private String location;
    private String lat;
    private String lng;
    private String sell_buyId;
    private int count;
    private String lastSeenTimeStamp;
    private String timeStamp;


    public NotificationAdDetails(String schoolName, String className, String location, String lat, String lng,
                                 String sell_buyId, int count, String lastSeenTimeStamp, String timeStamp) {
        this.schoolName = schoolName;
        this.className = className;
        this.location = location;
        this.lat = lat;
        this.lng = lng;
        this.sell_buyId = sell_buyId;
        this.count = count;
        this.lastSeenTimeStamp = lastSeenTimeStamp;
        this.timeStamp = timeStamp;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getClassName() {
        return className;
    }

    public String getLocation() {
        return location;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getSell_buyId() {
        return sell_buyId;
    }

    public int getCount() {
        return count;
    }

    public String getLastSeenTimeStamp() {
        return lastSeenTimeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
