package com.example.rishabh.reutilizar.Models;


import java.io.Serializable;

/**
 * Created by rishabh on 9/6/17.
 */

public class OneBookDetail implements Serializable {
    public boolean checkBox;
    public String title;
    public String condition;
    public String bookPath;
    public String description;
    public boolean owner ; //owner=true means view created by us which has title uneditable
    public String bookUrl;
    public String bookId;
    public boolean isImageUpload;

    public OneBookDetail(){
        description="";
        bookPath ="";
        bookUrl = "";
        bookId="";
    }

    public OneBookDetail(String title, String condition, String bookPath, String description, String bookUrl, String bookId) {
        this.title = title;
        this.condition = condition;
        this.bookPath = bookPath;
        this.description = description;
        this.bookUrl = bookUrl;
        this.bookId = bookId;
        this.isImageUpload = false;
    }
}
