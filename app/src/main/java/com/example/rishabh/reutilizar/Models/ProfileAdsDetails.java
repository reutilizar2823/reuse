package com.example.rishabh.reutilizar.Models;

import java.io.Serializable;

/**
 * Created by rishabh on 10/8/17.
 */

public class ProfileAdsDetails implements Serializable{
    private String schoolName;
    private String className;
    private String location;
    private String lat;
    private String lng;
    private String sell_buyId;
    private String dateTime;
    //Only for sell ProfileAdDetails
    private String price;
    private String status;

    //For Sell Ads
    public ProfileAdsDetails(String schoolName, String className, String location, String lat, String lng, String status,
    String sell_buyId , String price,String dateTime) {
        this.schoolName = schoolName;
        this.className = className;
        this.location = location;
        this.lat = lat;
        this.lng = lng;
        this.status = status;
        this.sell_buyId=sell_buyId;
        this.price = price;
        this.dateTime = dateTime;
    }

    //For Buy Ads
    public ProfileAdsDetails(String schoolName, String className, String location, String lat, String lng,
                             String sell_buyId,String dateTime ) {
        this.schoolName = schoolName;
        this.className = className;
        this.location = location;
        this.lat = lat;
        this.lng = lng;
        this.sell_buyId=sell_buyId;
        this.dateTime = dateTime;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getClassName() {
        return className;
    }

    public String getLocation() {
        return location;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getStatus() {
        return status;
    }

    public String getSell_buyId() {
        return sell_buyId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getPrice() {
        return price;
    }
}

