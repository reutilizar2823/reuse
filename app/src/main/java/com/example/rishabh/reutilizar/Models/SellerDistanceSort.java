package com.example.rishabh.reutilizar.Models;

import java.util.Comparator;

/**
 * Created by rishabh on 10/20/17.
 */

public class SellerDistanceSort implements Comparator<SellersBasicInfo>{


    @Override
    public int compare(SellersBasicInfo sellersBasicInfo1, SellersBasicInfo sellersBasicInfo2) {
         if(sellersBasicInfo1.distance<sellersBasicInfo2.distance) return -1;
         else if(sellersBasicInfo1.distance>sellersBasicInfo2.distance) return 1;
         return 0;
    }
}
