package com.example.rishabh.reutilizar.Models;

import com.example.rishabh.reutilizar.Models.SellersBasicInfo;

/**
 * Created by rishabh on 9/10/17.
 */

public class SellerTimeSort implements java.util.Comparator<com.example.rishabh.reutilizar.Models.SellersBasicInfo> {

    @Override
    public int compare(SellersBasicInfo sellersBasicInfo1, SellersBasicInfo sellersBasicInfo2) {
        long timeStamp1 = Long.parseLong(sellersBasicInfo1.timeStamp);
        long timeStamp2 = Long.parseLong(sellersBasicInfo2.timeStamp);
        if(timeStamp1 < timeStamp2 ) return  1;
        else if(timeStamp1 > timeStamp2) return -1;
        return 0;
    }
}



