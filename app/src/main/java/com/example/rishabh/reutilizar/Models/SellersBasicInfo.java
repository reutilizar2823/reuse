package com.example.rishabh.reutilizar.Models;

import java.io.Serializable;

/**
 * Created by rishabh on 9/8/17.
 */

public class SellersBasicInfo implements Serializable{
    public String name;
    public String number;
    public String photoUrl;
    public double lat;
    public double lng;
    public String location;
    public String status;
    public String UID;
    public String sellID;
    public double distance;
    public String timeStamp;
    public String actualTime;
    public String price;


    public SellersBasicInfo(String name, String number, String photoUrl, double lat, double lng, String location,
                            String status, String UID,
                            String sellID , String timeStamp,String actualTime,String price) {
        this.name = name;
        this.number = number;
        this.photoUrl = photoUrl;
        this.lat = lat;
        this.lng = lng;
        this.location = location;
        this.status = status;
        this.UID = UID;
        this.sellID = sellID;
        this.timeStamp = timeStamp;
        this.actualTime=actualTime;
        this.price=price;
    }
}
