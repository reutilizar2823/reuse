package com.example.rishabh.reutilizar.Models;

import java.io.Serializable;

/**
 * Created by rishabh on 9/10/17.
 */

public class ShowBookDetail implements Serializable {
    public String bookId;
    public String bookUrl;
    public String condition;
    public String description;
    public String title;

    public ShowBookDetail() {
    }

    public ShowBookDetail(String bookId, String bookUrl, String condition, String description, String title) {
        this.bookId = bookId;
        this.bookUrl = bookUrl;
        this.condition = condition;
        this.description = description;
        this.title = title;
    }
}
