package com.example.rishabh.reutilizar.Models;

/**
 * Created by rishabh on 9/2/17.
 */

public  class User {
    static private String name;
    static private String number;
    static private String photoURL;
    static private String UID;
   //static private String photoPath;

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        User.name = name;
    }

    public static String getNumber() {
        return number;
    }

    public static void setNumber(String number) {
        User.number = number;
    }

    public static String getPhotoURL() {
        return photoURL;
    }

    public static void setPhotoURL(String photoURL) {
        User.photoURL = photoURL;
    }

    public static String getUID() {
        return UID;
    }

    public static void setUID(String UID) {
        User.UID = UID;
    }
}
