package com.example.rishabh.reutilizar.Models;

import java.io.Serializable;

/**
 * Created by rishabh on 9/8/17.
 */
public class UserRequestDetails implements Serializable {
    private String schoolName;
    private String className;
    private String location;
    private String lat;
    private String lng;

    public UserRequestDetails(String schoolName, String className, String location, String lat, String lng ) {
        this.schoolName=schoolName;
        this.className = className;
        this.location = location;
        this.lat = lat;
        this.lng = lng;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getClassName() {
        return className;
    }

    public String getLocation() {
        return location;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
