package com.example.rishabh.reutilizar.SpinnerDialog;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by rishabh on 1/4/18.
 */

public class MyArrayAdapter  extends ArrayAdapter<String>{

    private ArrayList<String> mainArrayList , filterArrayList;

    public MyArrayAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<String> items) {
        super(context, resource ,items);
        this.mainArrayList = items;
        filterArrayList = new ArrayList<>();
        filterArrayList.addAll(mainArrayList);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mainArrayList.clear();
        if (charText.length() == 0) {
            mainArrayList.addAll(filterArrayList);
        }
        else
        {
            for (String str : filterArrayList) {
                if (str.toLowerCase(Locale.getDefault()).contains(charText)) {
                    mainArrayList.add(str);
                }
            }
        }
        notifyDataSetChanged();
    }


}
