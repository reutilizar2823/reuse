package com.example.rishabh.reutilizar.SpinnerDialog;

/**
 * Created by rishabh on 1/2/18.
 */

public interface OnSpinnerItemClick {
    public void onClick(String item , int position);
}
